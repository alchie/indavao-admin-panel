<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Realestate_projects_pictures Model Class
 *
 * Manipulates `Realestate_projects_pictures` table on database

 CREATE TABLE `realestate_projects_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Realestate_projects_pictures_model extends CI_Model {

	protected $id;
	protected $project_id;
	protected $url;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('project_id','url');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['id'] = $this->id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			$query = $this->db->get_where('realestate_projects_pictures', array('id' => $this->id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('realestate_projects_pictures', $this->getData(), array('id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('realestate_projects_pictures', array('id' => $this->id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `project_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setProjectId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->project_id = ( ($value == '') && ($this->project_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['project_id'] = $this->project_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'project_id';
		}
		return $this;
	}

	/** 
	* Get the value of `project_id` variable
	* @access public
	* @return String;
	*/

	public function getProjectId() {
		return $this->project_id;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->url = ( ($value == '') && ($this->url != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['url'] = $this->url;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'url';
		}
		return $this;
	}

	/** 
	* Get the value of `url` variable
	* @access public
	* @return String;
	*/

	public function getUrl() {
		return $this->url;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('realestate_projects_pictures', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('realestate_projects_pictures', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('realestate_projects_pictures', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('realestate_projects_pictures', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('realestate_projects_pictures');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('realestate_projects_pictures');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('realestate_projects_pictures');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('id','project_id','url');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file realestate_projects_pictures_model.php */
/* Location: ./application/models/realestate_projects_pictures_model.php */