<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Movies Model Class
 *
 * Manipulates `Movies` table on database

 CREATE TABLE `movies` (
  `movie_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `movie_title` varchar(200) NOT NULL,
  `movie_description` text,
  `movie_year` int(10) DEFAULT NULL,
  `movie_lang` varchar(100) DEFAULT NULL,
  `movie_slug` varchar(200) DEFAULT NULL,
  `movie_active` int(1) DEFAULT '0',
  `movie_poster` text,
  `movie_imdb` text,
  `movie_tag` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Movies_model extends CI_Model {

	protected $movie_id;
	protected $movie_title;
	protected $movie_description;
	protected $movie_year;
	protected $movie_lang;
	protected $movie_slug;
	protected $movie_poster;
	protected $movie_imdb;
	protected $movie_tag;
	protected $movie_active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('movie_title');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_id = ( ($value == '') && ($this->movie_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_id'] = $this->movie_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_id';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_id` variable
	* @access public
	* @return String;
	*/

	public function getMovieId() {
		return $this->movie_id;
	}

	/**
	* Get row by `movie_id`
	* @param movie_id
	* @return QueryResult
	**/

	public function getByMovieId() {
		if($this->movie_id != '') {
			$query = $this->db->get_where('movies', array('movie_id' => $this->movie_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `movie_id`
	**/

	public function updateByMovieId() {
		if($this->movie_id != '') {
			$this->setExclude('movie_id');
			if( $this->getData() ) {
				return $this->db->update('movies', $this->getData(), array('movie_id' => $this->movie_id ) );
			}
		}
	}


	/**
	* Delete row by `movie_id`
	**/

	public function deleteByMovieId() {
		if($this->movie_id != '') {
			return $this->db->delete('movies', array('movie_id' => $this->movie_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieTitle($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_title = ( ($value == '') && ($this->movie_title != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_title'] = $this->movie_title;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_title';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_title` variable
	* @access public
	* @return String;
	*/

	public function getMovieTitle() {
		return $this->movie_title;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_description` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieDescription($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_description = ( ($value == '') && ($this->movie_description != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_description'] = $this->movie_description;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_description';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_description` variable
	* @access public
	* @return String;
	*/

	public function getMovieDescription() {
		return $this->movie_description;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_year` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieYear($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_year = ( ($value == '') && ($this->movie_year != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_year'] = $this->movie_year;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_year';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_year` variable
	* @access public
	* @return String;
	*/

	public function getMovieYear() {
		return $this->movie_year;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_lang` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieLang($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_lang = ( ($value == '') && ($this->movie_lang != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_lang'] = $this->movie_lang;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_lang';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_lang` variable
	* @access public
	* @return String;
	*/

	public function getMovieLang() {
		return $this->movie_lang;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_slug = ( ($value == '') && ($this->movie_slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_slug'] = $this->movie_slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_slug` variable
	* @access public
	* @return String;
	*/

	public function getMovieSlug() {
		return $this->movie_slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_poster` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMoviePoster($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_poster = ( ($value == '') && ($this->movie_poster != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_poster'] = $this->movie_poster;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_poster';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_poster` variable
	* @access public
	* @return String;
	*/

	public function getMoviePoster() {
		return $this->movie_poster;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_imdb` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieImdb($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_imdb = ( ($value == '') && ($this->movie_imdb != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_imdb'] = $this->movie_imdb;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_imdb';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_imdb` variable
	* @access public
	* @return String;
	*/

	public function getMovieImdb() {
		return $this->movie_imdb;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_tag` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieTag($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_tag = ( ($value == '') && ($this->movie_tag != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['movie_tag'] = $this->movie_tag;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_tag';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_tag` variable
	* @access public
	* @return String;
	*/

	public function getMovieTag() {
		return $this->movie_tag;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `movie_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMovieActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->movie_active = ( ($value == '') && ($this->movie_active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['movie_active'] = $this->movie_active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'movie_active';
		}
		return $this;
	}

	/** 
	* Get the value of `movie_active` variable
	* @access public
	* @return String;
	*/

	public function getMovieActive() {
		return $this->movie_active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('movies', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('movies', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('movies', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('movies', $this->getData() ) === TRUE ) {
				$this->movie_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('movies');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('movies');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('movies');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('movie_id','movie_title','movie_description','movie_year','movie_lang','movie_slug','movie_poster','movie_imdb','movie_tag','movie_active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file movies_model.php */
/* Location: ./application/models/movies_model.php */