<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users Model Class
 *
 * Manipulates `Users` table on database

 CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `level` varchar(45) DEFAULT 'free',
  `active` int(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT 'user',
  `date_joined` datetime DEFAULT NULL,
  `ip_joined` varchar(100) DEFAULT NULL,
  `referrer` bigint(20) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Users_model extends CI_Model {

	protected $id;
	protected $fullname;
	protected $email;
	protected $password;
	protected $user_id;
	protected $username;
	protected $level = 'free';
	protected $referrer;
	protected $active = '0';
	protected $activation_code;
	protected $type = 'user';
	protected $date_joined;
	protected $ip_joined;
	protected $last_login;
	protected $last_login_ip;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('fullname');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['id'] = $this->id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			$query = $this->db->get_where('users', array('id' => $this->id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('users', array('id' => $this->id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `fullname` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->fullname = ( ($value == '') && ($this->fullname != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['fullname'] = $this->fullname;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'fullname';
		}
		return $this;
	}

	/** 
	* Get the value of `fullname` variable
	* @access public
	* @return String;
	*/

	public function getFullname() {
		return $this->fullname;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->email = ( ($value == '') && ($this->email != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['email'] = $this->email;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'email';
		}
		return $this;
	}

	/** 
	* Get the value of `email` variable
	* @access public
	* @return String;
	*/

	public function getEmail() {
		return $this->email;
	}

	/**
	* Get row by `email`
	* @param email
	* @return QueryResult
	**/

	public function getByEmail() {
		if($this->email != '') {
			$query = $this->db->get_where('users', array('email' => $this->email), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `email`
	**/

	public function updateByEmail() {
		if($this->email != '') {
			$this->setExclude('email');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('email' => $this->email ) );
			}
		}
	}


	/**
	* Delete row by `email`
	**/

	public function deleteByEmail() {
		if($this->email != '') {
			return $this->db->delete('users', array('email' => $this->email ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `password` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPassword($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->password = ( ($value == '') && ($this->password != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['password'] = $this->password;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'password';
		}
		return $this;
	}

	/** 
	* Get the value of `password` variable
	* @access public
	* @return String;
	*/

	public function getPassword() {
		return $this->password;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['user_id'] = $this->user_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			$query = $this->db->get_where('users', array('user_id' => $this->user_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('users', array('user_id' => $this->user_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `username` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUsername($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->username = ( ($value == '') && ($this->username != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['username'] = $this->username;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'username';
		}
		return $this;
	}

	/** 
	* Get the value of `username` variable
	* @access public
	* @return String;
	*/

	public function getUsername() {
		return $this->username;
	}

	/**
	* Get row by `username`
	* @param username
	* @return QueryResult
	**/

	public function getByUsername() {
		if($this->username != '') {
			$query = $this->db->get_where('users', array('username' => $this->username), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `username`
	**/

	public function updateByUsername() {
		if($this->username != '') {
			$this->setExclude('username');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('username' => $this->username ) );
			}
		}
	}


	/**
	* Delete row by `username`
	**/

	public function deleteByUsername() {
		if($this->username != '') {
			return $this->db->delete('users', array('username' => $this->username ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `level` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLevel($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->level = ( ($value == '') && ($this->level != '') ) ? 'free' : $value;
		if( $setWhere ) {
			$this->where['level'] = $this->level;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'level';
		}
		return $this;
	}

	/** 
	* Get the value of `level` variable
	* @access public
	* @return String;
	*/

	public function getLevel() {
		return $this->level;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `referrer` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReferrer($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->referrer = ( ($value == '') && ($this->referrer != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['referrer'] = $this->referrer;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'referrer';
		}
		return $this;
	}

	/** 
	* Get the value of `referrer` variable
	* @access public
	* @return String;
	*/

	public function getReferrer() {
		return $this->referrer;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->active = ( ($value == '') && ($this->active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['active'] = $this->active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'active';
		}
		return $this;
	}

	/** 
	* Get the value of `active` variable
	* @access public
	* @return String;
	*/

	public function getActive() {
		return $this->active;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `activation_code` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setActivationCode($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->activation_code = ( ($value == '') && ($this->activation_code != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['activation_code'] = $this->activation_code;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'activation_code';
		}
		return $this;
	}

	/** 
	* Get the value of `activation_code` variable
	* @access public
	* @return String;
	*/

	public function getActivationCode() {
		return $this->activation_code;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->type = ( ($value == '') && ($this->type != '') ) ? 'user' : $value;
		if( $setWhere ) {
			$this->where['type'] = $this->type;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'type';
		}
		return $this;
	}

	/** 
	* Get the value of `type` variable
	* @access public
	* @return String;
	*/

	public function getType() {
		return $this->type;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `date_joined` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDateJoined($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->date_joined = ( ($value == '') && ($this->date_joined != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['date_joined'] = $this->date_joined;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'date_joined';
		}
		return $this;
	}

	/** 
	* Get the value of `date_joined` variable
	* @access public
	* @return String;
	*/

	public function getDateJoined() {
		return $this->date_joined;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `ip_joined` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setIpJoined($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->ip_joined = ( ($value == '') && ($this->ip_joined != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['ip_joined'] = $this->ip_joined;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'ip_joined';
		}
		return $this;
	}

	/** 
	* Get the value of `ip_joined` variable
	* @access public
	* @return String;
	*/

	public function getIpJoined() {
		return $this->ip_joined;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `last_login` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastLogin($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->last_login = ( ($value == '') && ($this->last_login != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['last_login'] = $this->last_login;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'last_login';
		}
		return $this;
	}

	/** 
	* Get the value of `last_login` variable
	* @access public
	* @return String;
	*/

	public function getLastLogin() {
		return $this->last_login;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `last_login_ip` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastLoginIp($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->last_login_ip = ( ($value == '') && ($this->last_login_ip != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['last_login_ip'] = $this->last_login_ip;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'last_login_ip';
		}
		return $this;
	}

	/** 
	* Get the value of `last_login_ip` variable
	* @access public
	* @return String;
	*/

	public function getLastLoginIp() {
		return $this->last_login_ip;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('users', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('users', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('users', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('users', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('users');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('users');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('users');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('id','fullname','email','password','user_id','username','level','referrer','active','activation_code','type','date_joined','ip_joined','last_login','last_login_ip');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file users_model.php */
/* Location: ./application/models/users_model.php */