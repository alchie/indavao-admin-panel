<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Tv_series Model Class
 *
 * Manipulates `Tv_series` table on database

 CREATE TABLE `tv_series` (
  `series_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `series_title` varchar(200) NOT NULL,
  `series_slug` varchar(200) DEFAULT NULL,
  `series_description` text,
  `series_year` int(10) DEFAULT NULL,
  `series_lang` varchar(10) DEFAULT NULL,
  `series_poster` text,
  `series_active` int(1) DEFAULT '0',
  `series_tag` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`series_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Tv_series_model extends CI_Model {

	protected $series_id;
	protected $series_title;
	protected $series_slug;
	protected $series_description;
	protected $series_year;
	protected $series_lang;
	protected $series_poster;
	protected $series_tag;
	protected $series_active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('series_title');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_id = ( ($value == '') && ($this->series_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_id'] = $this->series_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_id';
		}
		return $this;
	}

	/** 
	* Get the value of `series_id` variable
	* @access public
	* @return String;
	*/

	public function getSeriesId() {
		return $this->series_id;
	}

	/**
	* Get row by `series_id`
	* @param series_id
	* @return QueryResult
	**/

	public function getBySeriesId() {
		if($this->series_id != '') {
			$query = $this->db->get_where('tv_series', array('series_id' => $this->series_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `series_id`
	**/

	public function updateBySeriesId() {
		if($this->series_id != '') {
			$this->setExclude('series_id');
			if( $this->getData() ) {
				return $this->db->update('tv_series', $this->getData(), array('series_id' => $this->series_id ) );
			}
		}
	}


	/**
	* Delete row by `series_id`
	**/

	public function deleteBySeriesId() {
		if($this->series_id != '') {
			return $this->db->delete('tv_series', array('series_id' => $this->series_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesTitle($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_title = ( ($value == '') && ($this->series_title != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_title'] = $this->series_title;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_title';
		}
		return $this;
	}

	/** 
	* Get the value of `series_title` variable
	* @access public
	* @return String;
	*/

	public function getSeriesTitle() {
		return $this->series_title;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_slug = ( ($value == '') && ($this->series_slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_slug'] = $this->series_slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `series_slug` variable
	* @access public
	* @return String;
	*/

	public function getSeriesSlug() {
		return $this->series_slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_description` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesDescription($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_description = ( ($value == '') && ($this->series_description != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_description'] = $this->series_description;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_description';
		}
		return $this;
	}

	/** 
	* Get the value of `series_description` variable
	* @access public
	* @return String;
	*/

	public function getSeriesDescription() {
		return $this->series_description;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_year` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesYear($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_year = ( ($value == '') && ($this->series_year != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_year'] = $this->series_year;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_year';
		}
		return $this;
	}

	/** 
	* Get the value of `series_year` variable
	* @access public
	* @return String;
	*/

	public function getSeriesYear() {
		return $this->series_year;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_lang` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesLang($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_lang = ( ($value == '') && ($this->series_lang != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_lang'] = $this->series_lang;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_lang';
		}
		return $this;
	}

	/** 
	* Get the value of `series_lang` variable
	* @access public
	* @return String;
	*/

	public function getSeriesLang() {
		return $this->series_lang;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_poster` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesPoster($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_poster = ( ($value == '') && ($this->series_poster != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_poster'] = $this->series_poster;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_poster';
		}
		return $this;
	}

	/** 
	* Get the value of `series_poster` variable
	* @access public
	* @return String;
	*/

	public function getSeriesPoster() {
		return $this->series_poster;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_tag` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesTag($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_tag = ( ($value == '') && ($this->series_tag != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['series_tag'] = $this->series_tag;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_tag';
		}
		return $this;
	}

	/** 
	* Get the value of `series_tag` variable
	* @access public
	* @return String;
	*/

	public function getSeriesTag() {
		return $this->series_tag;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `series_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSeriesActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->series_active = ( ($value == '') && ($this->series_active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['series_active'] = $this->series_active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'series_active';
		}
		return $this;
	}

	/** 
	* Get the value of `series_active` variable
	* @access public
	* @return String;
	*/

	public function getSeriesActive() {
		return $this->series_active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('tv_series', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('tv_series', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('tv_series', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('tv_series', $this->getData() ) === TRUE ) {
				$this->series_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('tv_series');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('tv_series');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('tv_series');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('series_id','series_title','series_slug','series_description','series_year','series_lang','series_poster','series_tag','series_active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file tv_series_model.php */
/* Location: ./application/models/tv_series_model.php */