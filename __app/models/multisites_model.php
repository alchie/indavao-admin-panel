<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Multisites Model Class
 *
 * Manipulates `Multisites` table on database

 CREATE TABLE `multisites` (
  `site_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Multisites_model extends CI_Model {

	protected $site_id;
	protected $site_name;
	protected $title;
	protected $description;
	protected $url;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('site_name','title','url');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `site_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSiteId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->site_id = ( ($value == '') && ($this->site_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['site_id'] = $this->site_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'site_id';
		}
		return $this;
	}

	/** 
	* Get the value of `site_id` variable
	* @access public
	* @return String;
	*/

	public function getSiteId() {
		return $this->site_id;
	}

	/**
	* Get row by `site_id`
	* @param site_id
	* @return QueryResult
	**/

	public function getBySiteId() {
		if($this->site_id != '') {
			$query = $this->db->get_where('multisites', array('site_id' => $this->site_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `site_id`
	**/

	public function updateBySiteId() {
		if($this->site_id != '') {
			$this->setExclude('site_id');
			if( $this->getData() ) {
				return $this->db->update('multisites', $this->getData(), array('site_id' => $this->site_id ) );
			}
		}
	}


	/**
	* Delete row by `site_id`
	**/

	public function deleteBySiteId() {
		if($this->site_id != '') {
			return $this->db->delete('multisites', array('site_id' => $this->site_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `site_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSiteName($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->site_name = ( ($value == '') && ($this->site_name != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['site_name'] = $this->site_name;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'site_name';
		}
		return $this;
	}

	/** 
	* Get the value of `site_name` variable
	* @access public
	* @return String;
	*/

	public function getSiteName() {
		return $this->site_name;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTitle($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->title = ( ($value == '') && ($this->title != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['title'] = $this->title;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'title';
		}
		return $this;
	}

	/** 
	* Get the value of `title` variable
	* @access public
	* @return String;
	*/

	public function getTitle() {
		return $this->title;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `description` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDescription($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->description = ( ($value == '') && ($this->description != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['description'] = $this->description;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'description';
		}
		return $this;
	}

	/** 
	* Get the value of `description` variable
	* @access public
	* @return String;
	*/

	public function getDescription() {
		return $this->description;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->url = ( ($value == '') && ($this->url != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['url'] = $this->url;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'url';
		}
		return $this;
	}

	/** 
	* Get the value of `url` variable
	* @access public
	* @return String;
	*/

	public function getUrl() {
		return $this->url;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('multisites', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('multisites', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('multisites', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('multisites', $this->getData() ) === TRUE ) {
				$this->site_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('multisites');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('multisites');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('multisites');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('site_id','site_name','title','description','url');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file multisites_model.php */
/* Location: ./application/models/multisites_model.php */