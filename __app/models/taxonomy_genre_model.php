<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy_genre Model Class
 *
 * Manipulates `Taxonomy_genre` table on database

 CREATE TABLE `taxonomy_genre` (
  `genre_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(200) NOT NULL,
  `genre_slug` varchar(200) DEFAULT NULL,
  `genre_active` int(1) DEFAULT '0',
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Taxonomy_genre_model extends CI_Model {

	protected $genre_id;
	protected $genre_name;
	protected $genre_slug;
	protected $genre_active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('genre_name');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `genre_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGenreId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->genre_id = ( ($value == '') && ($this->genre_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['genre_id'] = $this->genre_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'genre_id';
		}
		return $this;
	}

	/** 
	* Get the value of `genre_id` variable
	* @access public
	* @return String;
	*/

	public function getGenreId() {
		return $this->genre_id;
	}

	/**
	* Get row by `genre_id`
	* @param genre_id
	* @return QueryResult
	**/

	public function getByGenreId() {
		if($this->genre_id != '') {
			$query = $this->db->get_where('taxonomy_genre', array('genre_id' => $this->genre_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `genre_id`
	**/

	public function updateByGenreId() {
		if($this->genre_id != '') {
			$this->setExclude('genre_id');
			if( $this->getData() ) {
				return $this->db->update('taxonomy_genre', $this->getData(), array('genre_id' => $this->genre_id ) );
			}
		}
	}


	/**
	* Delete row by `genre_id`
	**/

	public function deleteByGenreId() {
		if($this->genre_id != '') {
			return $this->db->delete('taxonomy_genre', array('genre_id' => $this->genre_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `genre_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGenreName($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->genre_name = ( ($value == '') && ($this->genre_name != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['genre_name'] = $this->genre_name;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'genre_name';
		}
		return $this;
	}

	/** 
	* Get the value of `genre_name` variable
	* @access public
	* @return String;
	*/

	public function getGenreName() {
		return $this->genre_name;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `genre_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGenreSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->genre_slug = ( ($value == '') && ($this->genre_slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['genre_slug'] = $this->genre_slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'genre_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `genre_slug` variable
	* @access public
	* @return String;
	*/

	public function getGenreSlug() {
		return $this->genre_slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `genre_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGenreActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->genre_active = ( ($value == '') && ($this->genre_active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['genre_active'] = $this->genre_active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'genre_active';
		}
		return $this;
	}

	/** 
	* Get the value of `genre_active` variable
	* @access public
	* @return String;
	*/

	public function getGenreActive() {
		return $this->genre_active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('taxonomy_genre', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('taxonomy_genre', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('taxonomy_genre', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('taxonomy_genre', $this->getData() ) === TRUE ) {
				$this->genre_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('taxonomy_genre');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('taxonomy_genre');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('taxonomy_genre');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('genre_id','genre_name','genre_slug','genre_active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file taxonomy_genre_model.php */
/* Location: ./application/models/taxonomy_genre_model.php */