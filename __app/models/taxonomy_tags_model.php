<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy_tags Model Class
 *
 * Manipulates `Taxonomy_tags` table on database

 CREATE TABLE `taxonomy_tags` (
  `tag_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(100) NOT NULL,
  `tag_slug` varchar(100) DEFAULT NULL,
  `tag_active` int(1) DEFAULT '1',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Taxonomy_tags_model extends CI_Model {

	protected $tag_id;
	protected $tag_name;
	protected $tag_slug;
	protected $tag_active = '1';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('tag_name');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `tag_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTagId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->tag_id = ( ($value == '') && ($this->tag_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['tag_id'] = $this->tag_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'tag_id';
		}
		return $this;
	}

	/** 
	* Get the value of `tag_id` variable
	* @access public
	* @return String;
	*/

	public function getTagId() {
		return $this->tag_id;
	}

	/**
	* Get row by `tag_id`
	* @param tag_id
	* @return QueryResult
	**/

	public function getByTagId() {
		if($this->tag_id != '') {
			$query = $this->db->get_where('taxonomy_tags', array('tag_id' => $this->tag_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `tag_id`
	**/

	public function updateByTagId() {
		if($this->tag_id != '') {
			$this->setExclude('tag_id');
			if( $this->getData() ) {
				return $this->db->update('taxonomy_tags', $this->getData(), array('tag_id' => $this->tag_id ) );
			}
		}
	}


	/**
	* Delete row by `tag_id`
	**/

	public function deleteByTagId() {
		if($this->tag_id != '') {
			return $this->db->delete('taxonomy_tags', array('tag_id' => $this->tag_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `tag_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTagName($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->tag_name = ( ($value == '') && ($this->tag_name != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['tag_name'] = $this->tag_name;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'tag_name';
		}
		return $this;
	}

	/** 
	* Get the value of `tag_name` variable
	* @access public
	* @return String;
	*/

	public function getTagName() {
		return $this->tag_name;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `tag_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTagSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->tag_slug = ( ($value == '') && ($this->tag_slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['tag_slug'] = $this->tag_slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'tag_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `tag_slug` variable
	* @access public
	* @return String;
	*/

	public function getTagSlug() {
		return $this->tag_slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `tag_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTagActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->tag_active = ( ($value == '') && ($this->tag_active != '') ) ? '1' : $value;
		if( $setWhere ) {
			$this->where['tag_active'] = $this->tag_active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'tag_active';
		}
		return $this;
	}

	/** 
	* Get the value of `tag_active` variable
	* @access public
	* @return String;
	*/

	public function getTagActive() {
		return $this->tag_active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('taxonomy_tags', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('taxonomy_tags', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('taxonomy_tags', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('taxonomy_tags', $this->getData() ) === TRUE ) {
				$this->tag_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('taxonomy_tags');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('taxonomy_tags');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('taxonomy_tags');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('tag_id','tag_name','tag_slug','tag_active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file taxonomy_tags_model.php */
/* Location: ./application/models/taxonomy_tags_model.php */