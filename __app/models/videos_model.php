<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Videos Model Class
 *
 * Manipulates `Videos` table on database

 CREATE TABLE `videos` (
  `video_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video_title` varchar(200) NOT NULL,
  `video_slug` varchar(200) DEFAULT NULL,
  `video_url` text NOT NULL,
  `video_source` text,
  `video_image` text,
  `video_description` text,
  `video_parent` bigint(20) DEFAULT '0',
  `video_active` tinyint(1) DEFAULT '0',
  `video_added` datetime DEFAULT NULL,
  `video_priority` int(10) DEFAULT '0',
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Videos_model extends CI_Model {

	protected $video_id;
	protected $video_url;
	protected $video_title;
	protected $video_description;
	protected $video_image;
	protected $video_source;
	protected $video_parent = '0';
	protected $video_added;
	protected $video_priority = '0';
	protected $video_slug;
	protected $video_active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('video_url','video_title');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_id = ( ($value == '') && ($this->video_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_id'] = $this->video_id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_id';
		}
		return $this;
	}

	/** 
	* Get the value of `video_id` variable
	* @access public
	* @return String;
	*/

	public function getVideoId() {
		return $this->video_id;
	}

	/**
	* Get row by `video_id`
	* @param video_id
	* @return QueryResult
	**/

	public function getByVideoId() {
		if($this->video_id != '') {
			$query = $this->db->get_where('videos', array('video_id' => $this->video_id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `video_id`
	**/

	public function updateByVideoId() {
		if($this->video_id != '') {
			$this->setExclude('video_id');
			if( $this->getData() ) {
				return $this->db->update('videos', $this->getData(), array('video_id' => $this->video_id ) );
			}
		}
	}


	/**
	* Delete row by `video_id`
	**/

	public function deleteByVideoId() {
		if($this->video_id != '') {
			return $this->db->delete('videos', array('video_id' => $this->video_id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoUrl($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_url = ( ($value == '') && ($this->video_url != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_url'] = $this->video_url;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_url';
		}
		return $this;
	}

	/** 
	* Get the value of `video_url` variable
	* @access public
	* @return String;
	*/

	public function getVideoUrl() {
		return $this->video_url;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoTitle($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_title = ( ($value == '') && ($this->video_title != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_title'] = $this->video_title;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_title';
		}
		return $this;
	}

	/** 
	* Get the value of `video_title` variable
	* @access public
	* @return String;
	*/

	public function getVideoTitle() {
		return $this->video_title;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_description` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoDescription($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_description = ( ($value == '') && ($this->video_description != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_description'] = $this->video_description;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_description';
		}
		return $this;
	}

	/** 
	* Get the value of `video_description` variable
	* @access public
	* @return String;
	*/

	public function getVideoDescription() {
		return $this->video_description;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_image` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoImage($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_image = ( ($value == '') && ($this->video_image != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_image'] = $this->video_image;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_image';
		}
		return $this;
	}

	/** 
	* Get the value of `video_image` variable
	* @access public
	* @return String;
	*/

	public function getVideoImage() {
		return $this->video_image;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_source` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoSource($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_source = ( ($value == '') && ($this->video_source != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_source'] = $this->video_source;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_source';
		}
		return $this;
	}

	/** 
	* Get the value of `video_source` variable
	* @access public
	* @return String;
	*/

	public function getVideoSource() {
		return $this->video_source;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_parent` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoParent($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_parent = ( ($value == '') && ($this->video_parent != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['video_parent'] = $this->video_parent;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_parent';
		}
		return $this;
	}

	/** 
	* Get the value of `video_parent` variable
	* @access public
	* @return String;
	*/

	public function getVideoParent() {
		return $this->video_parent;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_added` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoAdded($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_added = ( ($value == '') && ($this->video_added != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_added'] = $this->video_added;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_added';
		}
		return $this;
	}

	/** 
	* Get the value of `video_added` variable
	* @access public
	* @return String;
	*/

	public function getVideoAdded() {
		return $this->video_added;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_priority` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoPriority($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_priority = ( ($value == '') && ($this->video_priority != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['video_priority'] = $this->video_priority;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_priority';
		}
		return $this;
	}

	/** 
	* Get the value of `video_priority` variable
	* @access public
	* @return String;
	*/

	public function getVideoPriority() {
		return $this->video_priority;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_slug = ( ($value == '') && ($this->video_slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['video_slug'] = $this->video_slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `video_slug` variable
	* @access public
	* @return String;
	*/

	public function getVideoSlug() {
		return $this->video_slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `video_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVideoActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->video_active = ( ($value == '') && ($this->video_active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['video_active'] = $this->video_active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'video_active';
		}
		return $this;
	}

	/** 
	* Get the value of `video_active` variable
	* @access public
	* @return String;
	*/

	public function getVideoActive() {
		return $this->video_active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('videos', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('videos', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('videos', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('videos', $this->getData() ) === TRUE ) {
				$this->video_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('videos');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('videos');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('videos');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('video_id','video_url','video_title','video_description','video_image','video_source','video_parent','video_added','video_priority','video_slug','video_active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file videos_model.php */
/* Location: ./application/models/videos_model.php */