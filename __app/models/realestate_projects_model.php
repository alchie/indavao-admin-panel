<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Realestate_projects Model Class
 *
 * Manipulates `Realestate_projects` table on database

 CREATE TABLE `realestate_projects` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `location` bigint(20) DEFAULT NULL,
  `devoper_name` varchar(200) DEFAULT NULL,
  `developer_url` text,
  `description` text,
  `map` text,
  `logo_url` text,
  `slug` varchar(200) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

 * @package		Model
 * @author		Chester Alan Tagudin
 * @link		http://www.chesteralan.com/
 */
 
class Realestate_projects_model extends CI_Model {

	protected $id;
	protected $name;
	protected $address;
	protected $location;
	protected $devoper_name;
	protected $developer_url;
	protected $description;
	protected $map;
	protected $logo_url;
	protected $slug;
	protected $active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array('name');
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['id'] = $this->id;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			$query = $this->db->get_where('realestate_projects', array('id' => $this->id), 1, 0);
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('realestate_projects', $this->getData(), array('id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('realestate_projects', array('id' => $this->id ) );
		}
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->name = ( ($value == '') && ($this->name != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['name'] = $this->name;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'name';
		}
		return $this;
	}

	/** 
	* Get the value of `name` variable
	* @access public
	* @return String;
	*/

	public function getName() {
		return $this->name;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `address` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setAddress($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->address = ( ($value == '') && ($this->address != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['address'] = $this->address;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'address';
		}
		return $this;
	}

	/** 
	* Get the value of `address` variable
	* @access public
	* @return String;
	*/

	public function getAddress() {
		return $this->address;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `location` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLocation($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->location = ( ($value == '') && ($this->location != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['location'] = $this->location;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'location';
		}
		return $this;
	}

	/** 
	* Get the value of `location` variable
	* @access public
	* @return String;
	*/

	public function getLocation() {
		return $this->location;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `devoper_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDevoperName($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->devoper_name = ( ($value == '') && ($this->devoper_name != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['devoper_name'] = $this->devoper_name;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'devoper_name';
		}
		return $this;
	}

	/** 
	* Get the value of `devoper_name` variable
	* @access public
	* @return String;
	*/

	public function getDevoperName() {
		return $this->devoper_name;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `developer_url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDeveloperUrl($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->developer_url = ( ($value == '') && ($this->developer_url != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['developer_url'] = $this->developer_url;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'developer_url';
		}
		return $this;
	}

	/** 
	* Get the value of `developer_url` variable
	* @access public
	* @return String;
	*/

	public function getDeveloperUrl() {
		return $this->developer_url;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `description` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDescription($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->description = ( ($value == '') && ($this->description != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['description'] = $this->description;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'description';
		}
		return $this;
	}

	/** 
	* Get the value of `description` variable
	* @access public
	* @return String;
	*/

	public function getDescription() {
		return $this->description;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `map` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMap($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->map = ( ($value == '') && ($this->map != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['map'] = $this->map;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'map';
		}
		return $this;
	}

	/** 
	* Get the value of `map` variable
	* @access public
	* @return String;
	*/

	public function getMap() {
		return $this->map;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `logo_url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLogoUrl($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->logo_url = ( ($value == '') && ($this->logo_url != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['logo_url'] = $this->logo_url;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'logo_url';
		}
		return $this;
	}

	/** 
	* Get the value of `logo_url` variable
	* @access public
	* @return String;
	*/

	public function getLogoUrl() {
		return $this->logo_url;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->slug = ( ($value == '') && ($this->slug != '') ) ? '' : $value;
		if( $setWhere ) {
			$this->where['slug'] = $this->slug;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'slug';
		}
		return $this;
	}

	/** 
	* Get the value of `slug` variable
	* @access public
	* @return String;
	*/

	public function getSlug() {
		return $this->slug;
	}

	// --------------------------------------------------------------------

	/** 
	* Sets a value to `active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE) {
		$this->active = ( ($value == '') && ($this->active != '') ) ? '0' : $value;
		if( $setWhere ) {
			$this->where['active'] = $this->active;
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'active';
		}
		return $this;
	}

	/** 
	* Get the value of `active` variable
	* @access public
	* @return String;
	*/

	public function getActive() {
		return $this->active;
	}


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->getWhere() ) {
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->db->where( $this->getWhere() );
			$query = $this->db->get('realestate_projects', 1, 0);
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->getWhere() ) {
				return $this->db->delete('realestate_projects', $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->getWhere() ) ) {
				return $this->db->update('realestate_projects', $this->getData(), $this->getWhere() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('realestate_projects', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		$this->db->limit( 1,$this->start);
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('realestate_projects');
		$result = $query->result();
		if( isset($result[0]) ) {
			return $result[0];
		}
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
		if( $this->getWhere() ) {
			$this->db->where($this->getWhere() ); 
		}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		$query = $this->db->get('realestate_projects');
		return $query->result();
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
			if( $this->join ) {
				foreach( $this->join as $join ) {
					$this->db->join( $join['table'], $join['connection'], $join['option'] );
				}
			}
			$this->where[$match] = $find;
			if( $this->where ) {
				$this->db->where($this->where); 
			}
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
			$query = $this->db->get('realestate_projects');
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->id, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	private function getData($exclude=NULL) {
		$data = array();

		$fields = array('id','name','address','location','devoper_name','developer_url','description','map','logo_url','slug','active');
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Set Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setWhere($where) {
		if($where != '') {
			if( ! is_array($where) ) {
				$this->where = array($where => $this->$where);
			} else {
				$h = array();
				foreach($where as $w) {
					if($w != '') {
						$h[$w] = $this->$w;
					}
				}
				$this->where = $h;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Get Where Clause 
	* @access public
	* @return Array;
	*/
	private function getWhere() {
		$where = array_merge( $this->where, $this->filter );
		if( ( $where ) && ( is_array( $where ) ) ) {
			return $where;
		} 
	}

	// --------------------------------------------------------------------

	/**
	* Clear Where 
	* @access public
	* @return Array;
	*/
	public function clearWhere() {
		unset($this->where);
		$this->where = array();
	}

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value) {
		$this->filter[$field] = $value;
	}


	public function clearFilter() {
		unset($this->filter);
		$this->filter = array();
	}

	// --------------------------------------------------------------------

	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select) {
		$this->select[] = $select;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

}

/* End of file realestate_projects_model.php */
/* Location: ./application/models/realestate_projects_model.php */