<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    
    function __construct() 
    {
        parent::__construct();
        
        $this->load->helper( array('url', 'form') );
        
         if ( !  $this->session->userdata('admin_logged_in') ) {
            $this->session->sess_destroy();
            redirect('login', 'location', 301);
            exit;
         }        
        
        // libraries
        $this->load->library('template_data');
        
        $this->template_data->set('active_session', $this->session->all_userdata() );
        $this->template_data->set('main_page', 'dashboard' );
        $this->template_data->set('sub_page', 'dashboard' ); 
        
        //$this->output->enable_profiler(TRUE);       

    }

}

