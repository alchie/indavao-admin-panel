        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo $base_url; ?>assets/js/jquery-1.10.2.js"></script>
     <script src="<?php echo $base_url; ?>assets/js/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>


    <script src="<?php echo $base_url; ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
    
    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo $base_url; ?>assets/js/sb-admin.js"></script>

    
    <script>
    $(document).ready(function() {
        $('#dataTables').dataTable();
        $('.btn-delete').click(function() {
            var conf = confirm('Are you sure?');
            if(conf) {
                return true;
            }
            return false;
        });
        $('.datetimepicker').datetimepicker();
    });
    </script>

</body>

</html>
