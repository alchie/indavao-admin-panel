<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('movies/edit/' . $this->input->get($filter_key) ); ?>">Movies</a></li><li class="active"><a href="<?php echo site_url("movies_genre") . "?movie_id=" . $this->input->get($filter_key); ?>">Genre</a></li><li ><a href="<?php echo site_url("movies_tags") . "?movie_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_movies_genre')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "movies_genre/add") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-default btn-sm pull-right">Add Genre</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Genre</th>
            
<?php  if(  $this->session->userdata('controller_movies_genre')->can_edit || $this->session->userdata('controller_movies_genre')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($movies_genre as $movies_genre_list) { ?>
        <tr class="">
            <td><?php echo $movies_genre_list->genre_name; ?></td>
 
<?php  if(  $this->session->userdata('controller_movies_genre')->can_edit || $this->session->userdata('controller_movies_genre')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_movies_genre')->can_edit   ) { ?>
<a href="<?php echo site_url( 'movies_genre/edit/' . $movies_genre_list->id) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_movies_genre')->can_delete   ) { ?>
<a href="<?php echo site_url( 'movies_genre/delete/' . $movies_genre_list->id ) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Genre</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="movie_id" id="movies_genre_movie_id" value="<?php echo $this->input->get('movie_id'); ?>" />

<div class="form-group"><label for="movies_genre_genre_id">Genre</label><select name="genre_id" id="movies_genre_genre_id" class="selectpicker form-control " placeholder="Genre" data-live-search="true"><option value="">- - Select Genre - -</option><?php 
function taxonomy_genre_WZF($taxonomy_genre_genre_id, $current='', $prefix = '') {
    foreach($taxonomy_genre_genre_id as $taxonomy_genre_WZF) {
        $selected = '';
        echo '<option '.$selected.' value="'.$taxonomy_genre_WZF->genre_id.'">'.  $prefix . ' ' . $taxonomy_genre_WZF->genre_name.'</option>';
        if( isset( $taxonomy_genre_WZF->children ) && (count( $taxonomy_genre_WZF->children ) > 0 ) ) {
            taxonomy_genre_WZF( $taxonomy_genre_WZF->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_genre_WZF( $taxonomy_genre_genre_id  );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies_genre") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('movies/edit/' . $this->input->get($filter_key) ); ?>">Movies</a></li><li class="active"><a href="<?php echo site_url("movies_genre") . "?movie_id=" . $this->input->get($filter_key); ?>">Genre</a></li><li ><a href="<?php echo site_url("movies_tags") . "?movie_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Genre</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="movies_genre_id" value="<?php echo $movies_genre->id; ?>" />

<input type="hidden" name="movie_id" id="movies_genre_movie_id" value="<?php echo $this->input->get('movie_id'); ?>" />

<div class="form-group"><label for="movies_genre_genre_id">Genre</label><select name="genre_id" id="movies_genre_genre_id" class="selectpicker form-control " placeholder="Genre" data-live-search="true"><option value="">- - Select Genre - -</option><?php 
function taxonomy_genre_NoC($taxonomy_genre_genre_id, $current='', $prefix = '') {
    foreach($taxonomy_genre_genre_id as $taxonomy_genre_NoC) {
        $selected = ($current->genre_id == $taxonomy_genre_NoC->genre_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$taxonomy_genre_NoC->genre_id.'">'.  $prefix . ' ' . $taxonomy_genre_NoC->genre_name.'</option>';
        if( isset( $taxonomy_genre_NoC->children ) && (count( $taxonomy_genre_NoC->children ) > 0 ) ) {
            taxonomy_genre_NoC( $taxonomy_genre_NoC->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_genre_NoC( $taxonomy_genre_genre_id , $movies_genre );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies_genre") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
