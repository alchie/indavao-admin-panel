<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_admins')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "admins/add"); ?>" class="btn btn-default btn-sm pull-right">Add Admins</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Email</th>
            <th>Username</th>
            <th>Date Created</th>
            
<?php  if(  $this->session->userdata('controller_admins')->can_edit || $this->session->userdata('controller_admins')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($admins as $admins_list) { ?>
        <tr class="<?php echo ($admins_list->active) ? '' : 'danger'; ?>">
            <td><?php echo $admins_list->email; ?></td>
            <td><?php echo $admins_list->username; ?></td>
            <td><?php echo $admins_list->date_created; ?></td>
 
<?php  if(  $this->session->userdata('controller_admins')->can_edit || $this->session->userdata('controller_admins')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_admins')->can_edit   ) { ?>
<a href="<?php echo site_url( 'admins/edit/' . $admins_list->id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_admins')->can_delete   ) { ?>
<a href="<?php echo site_url( 'admins/delete/' . $admins_list->id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Admins</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="admins_email">Email</label><input type="text" name="email" id="admins_email" class="form-control  text " placeholder="Email" value="<?php echo ($this->input->post('email')) ? $this->input->post('email') : ''; ?>" /></div>

<div class="form-group"><label for="admins_username">Username</label><input type="text" name="username" id="admins_username" class="form-control  text " placeholder="Username" value="<?php echo ($this->input->post('username')) ? $this->input->post('username') : ''; ?>" /></div>

<div class="form-group"><label for="admins_password">Password</label><input type="text" name="password" id="admins_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="confirm_admins_password">Confirm Password</label><input type="text" name="confirm_password" id="confirm_admins_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="active" id="admins_active" class="">Active</label></div></div>

<div class="form-group"><label for="admins_date_created">Date Created</label><input type="text" name="date_created" id="admins_date_created" class="form-control  text datetimepicker" placeholder="Date Created" value="<?php echo unix_to_human( time(), TRUE, 'eu' ); ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "admins"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#admins-tab">Admins</a></li><li><a href="<?php echo site_url("admins_access") . "?admin_id=" . $admins->id; ?>">Controller Access</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Admins</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="admins_id" value="<?php echo $admins->id; ?>" />

<div class="form-group"><label for="admins_email">Email</label><input type="text" name="email" id="admins_email" class="form-control  text " placeholder="Email" value="<?php echo $admins->email; ?>" /></div>

<div class="form-group"><label for="admins_username">Username</label><input type="text" name="username" id="admins_username" class="form-control  text " placeholder="Username" value="<?php echo $admins->username; ?>" /></div>

<div class="form-group"><label for="admins_password">Password</label><input type="text" name="password" id="admins_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="confirm_admins_password">Confirm Password</label><input type="text" name="confirm_password" id="confirm_admins_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($admins->active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="active" id="admins_active" class="">Active</label></div></div>

<div class="form-group"><label for="admins_date_created">Date Created</label><input type="text" name="date_created" id="admins_date_created" class="form-control  text datetimepicker" placeholder="Date Created" value="<?php echo ($admins->date_created != '') ? $admins->date_created : unix_to_human( time(), TRUE, 'eu' ); ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "admins"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
