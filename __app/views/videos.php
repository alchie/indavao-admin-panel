<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_videos')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "videos/add"); ?>" class="btn btn-default btn-sm pull-right">Add Video</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Title</th>
            
<?php  if(  $this->session->userdata('controller_videos')->can_edit || $this->session->userdata('controller_videos')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($videos as $videos_list) { ?>
        <tr class="<?php echo ($videos_list->video_active) ? '' : 'danger'; ?>">
            <td><?php echo $videos_list->video_title; ?></td>
 
<?php  if(  $this->session->userdata('controller_videos')->can_edit || $this->session->userdata('controller_videos')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_videos')->can_edit   ) { ?>
<a href="<?php echo site_url( 'videos/edit/' . $videos_list->video_id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_videos')->can_delete   ) { ?>
<a href="<?php echo site_url( 'videos/delete/' . $videos_list->video_id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Video</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="videos_video_url">URL</label><input type="text" name="video_url" id="videos_video_url" class="form-control  text " placeholder="URL" value="<?php echo ($this->input->post('video_url')) ? $this->input->post('video_url') : ''; ?>" /></div>

<div class="form-group"><label for="videos_video_title">Title</label><input type="text" name="video_title" id="videos_video_title" class="form-control  text " placeholder="Title" value="<?php echo ($this->input->post('video_title')) ? $this->input->post('video_title') : ''; ?>" /></div>

<div class="form-group"><label for="videos_video_description">Description</label><textarea name="video_description" id="videos_video_description" class="form-control  textarea" placeholder="Description" rows="5"></textarea></div>

<div class="form-group"><label for="videos_video_image">Image</label><input type="text" name="video_image" id="videos_video_image" class="form-control  text " placeholder="Image" value="<?php echo ($this->input->post('video_image')) ? $this->input->post('video_image') : ''; ?>" /></div>

<div class="form-group"><label for="videos_video_source">Source</label><select name="video_source" id="videos_video_source" class="selectpicker form-control " placeholder="Source" data-live-search="true"><option value="">- - Select Source - -</option><option  value="youtube">Youtube</option></select></div>

<div class="form-group"><label for="videos_video_parent">Parent</label><select name="video_parent" id="videos_video_parent" class="selectpicker form-control " placeholder="Parent" data-live-search="true"><option value="">- - Select Parent - -</option><?php 
function videos_ZAe($videos_video_parent, $current='', $prefix = '') {
    foreach($videos_video_parent as $videos_ZAe) {
        $selected = '';
        echo '<option '.$selected.' value="'.$videos_ZAe->video_id.'">'.  $prefix . ' ' . $videos_ZAe->video_title.'</option>';
        if( isset( $videos_ZAe->children ) && (count( $videos_ZAe->children ) > 0 ) ) {
            videos_ZAe( $videos_ZAe->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
videos_ZAe( $videos_video_parent  );
?></select></div>

<div class="form-group"><label for="videos_video_added">Date Added</label><input type="text" name="video_added" id="videos_video_added" class="form-control  text datetimepicker" placeholder="Date Added" value="<?php echo unix_to_human( time(), TRUE, 'eu' ); ?>" /></div>

<div class="form-group"><label for="videos_video_priority">Priority</label><input type="text" name="video_priority" id="videos_video_priority" class="form-control  text " placeholder="Priority" value="<?php echo ($this->input->post('video_priority')) ? $this->input->post('video_priority') : ''; ?>" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="video_active" id="videos_video_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "videos"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#videos-tab">Videos</a></li><li><a href="<?php echo site_url("videos_tag") . "?video_id=" . $videos->video_id; ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Videos</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="video_id" id="videos_video_id" value="<?php echo $videos->video_id; ?>" />

<div class="form-group"><label for="videos_video_url">URL</label><input type="text" name="video_url" id="videos_video_url" class="form-control  text " placeholder="URL" value="<?php echo $videos->video_url; ?>" /></div>

<div class="form-group"><label for="videos_video_title">Title</label><input type="text" name="video_title" id="videos_video_title" class="form-control  text " placeholder="Title" value="<?php echo $videos->video_title; ?>" /></div>

<div class="form-group"><label for="videos_video_description">Description</label><textarea name="video_description" id="videos_video_description" class="form-control  textarea" placeholder="Description" rows="5"><?php echo $videos->video_description; ?></textarea></div>

<div class="form-group"><label for="videos_video_image">Image</label><input type="text" name="video_image" id="videos_video_image" class="form-control  text " placeholder="Image" value="<?php echo $videos->video_image; ?>" /></div>

<div class="form-group"><label for="videos_video_source">Source</label><select name="video_source" id="videos_video_source" class="selectpicker form-control " placeholder="Source" data-live-search="true"><option value="">- - Select Source - -</option><option <?php echo ($videos->video_source == 'youtube') ? 'SELECTED' : ''; ?> value="youtube">Youtube</option></select></div>

<div class="form-group"><label for="videos_video_parent">Parent</label><select name="video_parent" id="videos_video_parent" class="selectpicker form-control " placeholder="Parent" data-live-search="true"><option value="">- - Select Parent - -</option><?php 
function videos_QwW($videos_video_parent, $current='', $prefix = '') {
    foreach($videos_video_parent as $videos_QwW) {
        $selected = ($current->video_parent == $videos_QwW->video_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$videos_QwW->video_id.'">'.  $prefix . ' ' . $videos_QwW->video_title.'</option>';
        if( isset( $videos_QwW->children ) && (count( $videos_QwW->children ) > 0 ) ) {
            videos_QwW( $videos_QwW->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
videos_QwW( $videos_video_parent , $videos );
?></select></div>

<div class="form-group"><label for="videos_video_added">Date Added</label><input type="text" name="video_added" id="videos_video_added" class="form-control  text datetimepicker" placeholder="Date Added" value="<?php echo ($videos->video_added != '') ? $videos->video_added : unix_to_human( time(), TRUE, 'eu' ); ?>" /></div>

<div class="form-group"><label for="videos_video_priority">Priority</label><input type="text" name="video_priority" id="videos_video_priority" class="form-control  text " placeholder="Priority" value="<?php echo $videos->video_priority; ?>" /></div>

<div class="form-group"><label for="videos_video_slug">Slug</label><input type="text" name="video_slug" id="videos_video_slug" class="form-control  text " placeholder="Slug" value="<?php echo $videos->video_slug; ?>" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($videos->video_active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="video_active" id="videos_video_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "videos"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
