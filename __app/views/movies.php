<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_movies')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "movies/add"); ?>" class="btn btn-default btn-sm pull-right">Add Movies</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Title</th>
            <th>Year</th>
            <th>Language</th>
            <th>Tag</th>
            
<?php  if(  $this->session->userdata('controller_movies')->can_edit || $this->session->userdata('controller_movies')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($movies as $movies_list) { ?>
        <tr class="<?php echo ($movies_list->movie_active) ? '' : 'danger'; ?>">
            <td><?php echo $movies_list->movie_title; ?></td>
            <td><a href="<?php echo site_url('movies/index/movie_year/' . $movies_list->movie_year ); ?>"><?php echo $movies_list->movie_year; ?></a></td>
            <td><a href="<?php echo site_url('movies/index/movie_lang/' . $movies_list->movie_lang ); ?>"><?php echo $movies_list->movie_lang; ?></a></td>
            <td><?php echo $movies_list->tag_name; ?></td>
 
<?php  if(  $this->session->userdata('controller_movies')->can_edit || $this->session->userdata('controller_movies')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_movies')->can_edit   ) { ?>
<a href="<?php echo site_url( 'movies/edit/' . $movies_list->movie_id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_movies')->can_delete   ) { ?>
<a href="<?php echo site_url( 'movies/delete/' . $movies_list->movie_id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Movies</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="movies_movie_title">Title</label><input type="text" name="movie_title" id="movies_movie_title" class="form-control  text " placeholder="Title" value="<?php echo ($this->input->post('movie_title')) ? $this->input->post('movie_title') : ''; ?>" /></div>

<div class="form-group"><label for="movies_movie_description">Description</label><textarea name="movie_description" id="movies_movie_description" class="form-control  textarea" placeholder="Description" rows="5"></textarea></div>

<div class="form-group"><label for="movies_movie_year">Year</label><input type="text" name="movie_year" id="movies_movie_year" class="form-control  text " placeholder="Year" value="<?php echo ($this->input->post('movie_year')) ? $this->input->post('movie_year') : ''; ?>" /></div>

<div class="form-group"><label for="movies_movie_lang">Language</label><input type="text" name="movie_lang" id="movies_movie_lang" class="form-control  text " placeholder="Language" value="<?php echo ($this->input->post('movie_lang')) ? $this->input->post('movie_lang') : ''; ?>" /></div>

<div class="form-group"><label for="movies_movie_poster">Poster</label><input type="text" name="movie_poster" id="movies_movie_poster" class="form-control  text " placeholder="Poster" value="<?php echo ($this->input->post('movie_poster')) ? $this->input->post('movie_poster') : ''; ?>" /></div>

<div class="form-group"><label for="movies_movie_imdb">IMDB URL</label><input type="text" name="movie_imdb" id="movies_movie_imdb" class="form-control  text " placeholder="IMDB URL" value="<?php echo ($this->input->post('movie_imdb')) ? $this->input->post('movie_imdb') : ''; ?>" /></div>

<div class="form-group"><label for="movies_movie_tag">Tag</label><select name="movie_tag" id="movies_movie_tag" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_JYE($taxonomy_tags_movie_tag, $current='', $prefix = '') {
    foreach($taxonomy_tags_movie_tag as $taxonomy_tags_JYE) {
        $selected = '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_JYE->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_JYE->tag_name.'</option>';
        if( isset( $taxonomy_tags_JYE->children ) && (count( $taxonomy_tags_JYE->children ) > 0 ) ) {
            taxonomy_tags_JYE( $taxonomy_tags_JYE->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_JYE( $taxonomy_tags_movie_tag  );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="movie_active" id="movies_movie_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#movies-tab">Movies</a></li><li><a href="<?php echo site_url("movies_genre") . "?movie_id=" . $movies->movie_id; ?>">Genre</a></li><li><a href="<?php echo site_url("movies_tags") . "?movie_id=" . $movies->movie_id; ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Movies</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="movie_id" id="movies_movie_id" value="<?php echo $movies->movie_id; ?>" />

<div class="form-group"><label for="movies_movie_title">Title</label><input type="text" name="movie_title" id="movies_movie_title" class="form-control  text " placeholder="Title" value="<?php echo $movies->movie_title; ?>" /></div>

<div class="form-group"><label for="movies_movie_description">Description</label><textarea name="movie_description" id="movies_movie_description" class="form-control  textarea" placeholder="Description" rows="5"><?php echo $movies->movie_description; ?></textarea></div>

<div class="form-group"><label for="movies_movie_year">Year</label><input type="text" name="movie_year" id="movies_movie_year" class="form-control  text " placeholder="Year" value="<?php echo $movies->movie_year; ?>" /></div>

<div class="form-group"><label for="movies_movie_lang">Language</label><input type="text" name="movie_lang" id="movies_movie_lang" class="form-control  text " placeholder="Language" value="<?php echo $movies->movie_lang; ?>" /></div>

<div class="form-group"><label for="movies_movie_slug">Slug</label><input type="text" name="movie_slug" id="movies_movie_slug" class="form-control  text " placeholder="Slug" value="<?php echo $movies->movie_slug; ?>" /></div>

<div class="form-group"><label for="movies_movie_poster">Poster</label><input type="text" name="movie_poster" id="movies_movie_poster" class="form-control  text " placeholder="Poster" value="<?php echo $movies->movie_poster; ?>" /></div>

<div class="form-group"><label for="movies_movie_imdb">IMDB URL</label><input type="text" name="movie_imdb" id="movies_movie_imdb" class="form-control  text " placeholder="IMDB URL" value="<?php echo $movies->movie_imdb; ?>" /></div>

<div class="form-group"><label for="movies_movie_tag">Tag</label><select name="movie_tag" id="movies_movie_tag" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_Zkc($taxonomy_tags_movie_tag, $current='', $prefix = '') {
    foreach($taxonomy_tags_movie_tag as $taxonomy_tags_Zkc) {
        $selected = ($current->movie_tag == $taxonomy_tags_Zkc->tag_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_Zkc->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_Zkc->tag_name.'</option>';
        if( isset( $taxonomy_tags_Zkc->children ) && (count( $taxonomy_tags_Zkc->children ) > 0 ) ) {
            taxonomy_tags_Zkc( $taxonomy_tags_Zkc->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_Zkc( $taxonomy_tags_movie_tag , $movies );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($movies->movie_active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="movie_active" id="movies_movie_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
