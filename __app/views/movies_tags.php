<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('movies/edit/' . $this->input->get($filter_key) ); ?>">Movies</a></li><li ><a href="<?php echo site_url("movies_genre") . "?movie_id=" . $this->input->get($filter_key); ?>">Genre</a></li><li class="active"><a href="<?php echo site_url("movies_tags") . "?movie_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_movies_tags')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "movies_tags/add") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-default btn-sm pull-right">Add Tag</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Tag</th>
            
<?php  if(  $this->session->userdata('controller_movies_tags')->can_edit || $this->session->userdata('controller_movies_tags')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($movies_tags as $movies_tags_list) { ?>
        <tr class="">
            <td><?php echo $movies_tags_list->tag_name; ?></td>
 
<?php  if(  $this->session->userdata('controller_movies_tags')->can_edit || $this->session->userdata('controller_movies_tags')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_movies_tags')->can_edit   ) { ?>
<a href="<?php echo site_url( 'movies_tags/edit/' . $movies_tags_list->id) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_movies_tags')->can_delete   ) { ?>
<a href="<?php echo site_url( 'movies_tags/delete/' . $movies_tags_list->id ) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Tag</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="movie_id" id="movies_tags_movie_id" value="<?php echo $this->input->get('movie_id'); ?>" />

<div class="form-group"><label for="movies_tags_tag_id">Tag</label><select name="tag_id" id="movies_tags_tag_id" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_aEt($taxonomy_tags_tag_id, $current='', $prefix = '') {
    foreach($taxonomy_tags_tag_id as $taxonomy_tags_aEt) {
        $selected = '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_aEt->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_aEt->tag_name.'</option>';
        if( isset( $taxonomy_tags_aEt->children ) && (count( $taxonomy_tags_aEt->children ) > 0 ) ) {
            taxonomy_tags_aEt( $taxonomy_tags_aEt->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_aEt( $taxonomy_tags_tag_id  );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies_tags") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('movies/edit/' . $this->input->get($filter_key) ); ?>">Movies</a></li><li ><a href="<?php echo site_url("movies_genre") . "?movie_id=" . $this->input->get($filter_key); ?>">Genre</a></li><li class="active"><a href="<?php echo site_url("movies_tags") . "?movie_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Tags</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="movies_tags_id" value="<?php echo $movies_tags->id; ?>" />

<input type="hidden" name="movie_id" id="movies_tags_movie_id" value="<?php echo $this->input->get('movie_id'); ?>" />

<div class="form-group"><label for="movies_tags_tag_id">Tag</label><select name="tag_id" id="movies_tags_tag_id" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_bcf($taxonomy_tags_tag_id, $current='', $prefix = '') {
    foreach($taxonomy_tags_tag_id as $taxonomy_tags_bcf) {
        $selected = ($current->tag_id == $taxonomy_tags_bcf->tag_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_bcf->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_bcf->tag_name.'</option>';
        if( isset( $taxonomy_tags_bcf->children ) && (count( $taxonomy_tags_bcf->children ) > 0 ) ) {
            taxonomy_tags_bcf( $taxonomy_tags_bcf->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_bcf( $taxonomy_tags_tag_id , $movies_tags );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "movies_tags") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
