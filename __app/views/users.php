<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_users')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "users/add"); ?>" class="btn btn-default btn-sm pull-right">Add User</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Fullname</th>
            <th>Email</th>
            <th>Username</th>
            
<?php  if(  $this->session->userdata('controller_users')->can_edit || $this->session->userdata('controller_users')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($users as $users_list) { ?>
        <tr class="<?php echo ($users_list->active) ? '' : 'danger'; ?>">
            <td><?php echo $users_list->fullname; ?></td>
            <td><?php echo $users_list->email; ?></td>
            <td><?php echo $users_list->username; ?></td>
 
<?php  if(  $this->session->userdata('controller_users')->can_edit || $this->session->userdata('controller_users')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_users')->can_edit   ) { ?>
<a href="<?php echo site_url( 'users/edit/' . $users_list->id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_users')->can_delete   ) { ?>
<a href="<?php echo site_url( 'users/delete/' . $users_list->id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add User</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="users_fullname">Fullname</label><input type="text" name="fullname" id="users_fullname" class="form-control  text " placeholder="Fullname" value="<?php echo ($this->input->post('fullname')) ? $this->input->post('fullname') : ''; ?>" /></div>

<div class="form-group"><label for="users_email">Email</label><input type="text" name="email" id="users_email" class="form-control  text " placeholder="Email" value="<?php echo ($this->input->post('email')) ? $this->input->post('email') : ''; ?>" /></div>

<div class="form-group"><label for="users_password">Password</label><input type="text" name="password" id="users_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="confirm_users_password">Confirm Password</label><input type="text" name="confirm_password" id="confirm_users_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="users_user_id">User ID</label><input type="text" name="user_id" id="users_user_id" class="form-control  text " placeholder="User ID" value="<?php echo random_string('alnum', 10); ?>" /></div>

<div class="form-group"><label for="users_username">Username</label><input type="text" name="username" id="users_username" class="form-control  text " placeholder="Username" value="<?php echo ($this->input->post('username')) ? $this->input->post('username') : ''; ?>" /></div>

<div class="form-group"><label for="users_level">Level</label><input type="text" name="level" id="users_level" class="form-control  text " placeholder="Level" value="<?php echo ($this->input->post('level')) ? $this->input->post('level') : ''; ?>" /></div>

<div class="form-group"><label for="users_referrer">Referrer</label><select name="referrer" id="users_referrer" class="selectpicker form-control " placeholder="Referrer" data-live-search="true"><option value="">- - Select Referrer - -</option><?php 
function users_Uac($users_referrer, $current='', $prefix = '') {
    foreach($users_referrer as $users_Uac) {
        $selected = '';
        echo '<option '.$selected.' value="'.$users_Uac->id.'">'.  $prefix . ' ' . $users_Uac->fullname.'</option>';
        if( isset( $users_Uac->children ) && (count( $users_Uac->children ) > 0 ) ) {
            users_Uac( $users_Uac->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
users_Uac( $users_referrer  );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="active" id="users_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "users"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#users-tab">Users</a></li><li><a href="<?php echo site_url("users_network") . "?user_id=" . $users->id; ?>"></a></li><li><a href="<?php echo site_url("users_points") . "?user_id=" . $users->id; ?>"></a></li><li><a href="<?php echo site_url("users_profile") . "?user_id=" . $users->id; ?>"></a></li><li><a href="<?php echo site_url("users_social_network") . "?user_id=" . $users->id; ?>"></a></li><li><a href="<?php echo site_url("users_stats") . "?user_id=" . $users->id; ?>"></a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Users</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="users_id" value="<?php echo $users->id; ?>" />

<div class="form-group"><label for="users_fullname">Fullname</label><input type="text" name="fullname" id="users_fullname" class="form-control  text " placeholder="Fullname" value="<?php echo $users->fullname; ?>" /></div>

<div class="form-group"><label for="users_email">Email</label><input type="text" name="email" id="users_email" class="form-control  text " placeholder="Email" value="<?php echo $users->email; ?>" /></div>

<div class="form-group"><label for="users_password">Password</label><input type="text" name="password" id="users_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="confirm_users_password">Confirm Password</label><input type="text" name="confirm_password" id="confirm_users_password" class="form-control  password" placeholder="Password" value="" /></div>

<div class="form-group"><label for="users_user_id">User ID</label><input type="text" name="user_id" id="users_user_id" class="form-control  text " placeholder="User ID" value="<?php echo ($users->user_id != '') ? $users->user_id : random_string('alnum', 10); ?>" /></div>

<div class="form-group"><label for="users_username">Username</label><input type="text" name="username" id="users_username" class="form-control  text " placeholder="Username" value="<?php echo $users->username; ?>" /></div>

<div class="form-group"><label for="users_level">Level</label><input type="text" name="level" id="users_level" class="form-control  text " placeholder="Level" value="<?php echo $users->level; ?>" /></div>

<div class="form-group"><label for="users_referrer">Referrer</label><select name="referrer" id="users_referrer" class="selectpicker form-control " placeholder="Referrer" data-live-search="true"><option value="">- - Select Referrer - -</option><?php 
function users_gKS($users_referrer, $current='', $prefix = '') {
    foreach($users_referrer as $users_gKS) {
        $selected = ($current->referrer == $users_gKS->id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$users_gKS->id.'">'.  $prefix . ' ' . $users_gKS->fullname.'</option>';
        if( isset( $users_gKS->children ) && (count( $users_gKS->children ) > 0 ) ) {
            users_gKS( $users_gKS->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
users_gKS( $users_referrer , $users );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($users->active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="active" id="users_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "users"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
