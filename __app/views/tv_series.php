<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_tv_series')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "tv_series/add"); ?>" class="btn btn-default btn-sm pull-right">Add Series</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Title</th>
            <th>Year</th>
            <th>Language</th>
            
<?php  if(  $this->session->userdata('controller_tv_series')->can_edit || $this->session->userdata('controller_tv_series')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($tv_series as $tv_series_list) { ?>
        <tr class="<?php echo ($tv_series_list->series_active) ? '' : 'danger'; ?>">
            <td><?php echo $tv_series_list->series_title; ?></td>
            <td><a href="<?php echo site_url('tv_series/index/series_year/' . $tv_series_list->series_year ); ?>"><?php echo $tv_series_list->series_year; ?></a></td>
            <td><a href="<?php echo site_url('tv_series/index/series_lang/' . $tv_series_list->series_lang ); ?>"><?php echo $tv_series_list->series_lang; ?></a></td>
 
<?php  if(  $this->session->userdata('controller_tv_series')->can_edit || $this->session->userdata('controller_tv_series')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_tv_series')->can_edit   ) { ?>
<a href="<?php echo site_url( 'tv_series/edit/' . $tv_series_list->series_id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_tv_series')->can_delete   ) { ?>
<a href="<?php echo site_url( 'tv_series/delete/' . $tv_series_list->series_id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Series</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="tv_series_series_title">Title</label><input type="text" name="series_title" id="tv_series_series_title" class="form-control  text " placeholder="Title" value="<?php echo ($this->input->post('series_title')) ? $this->input->post('series_title') : ''; ?>" /></div>

<div class="form-group"><label for="tv_series_series_description">Description</label><textarea name="series_description" id="tv_series_series_description" class="form-control  textarea" placeholder="Description" rows="5"></textarea></div>

<div class="form-group"><label for="tv_series_series_year">Year</label><input type="text" name="series_year" id="tv_series_series_year" class="form-control  text " placeholder="Year" value="<?php echo ($this->input->post('series_year')) ? $this->input->post('series_year') : ''; ?>" /></div>

<div class="form-group"><label for="tv_series_series_lang">Language</label><input type="text" name="series_lang" id="tv_series_series_lang" class="form-control  text " placeholder="Language" value="<?php echo ($this->input->post('series_lang')) ? $this->input->post('series_lang') : ''; ?>" /></div>

<div class="form-group"><label for="tv_series_series_poster">Picture</label><input type="text" name="series_poster" id="tv_series_series_poster" class="form-control  text " placeholder="Picture" value="<?php echo ($this->input->post('series_poster')) ? $this->input->post('series_poster') : ''; ?>" /></div>

<div class="form-group"><label for="tv_series_series_tag">Tag</label><select name="series_tag" id="tv_series_series_tag" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_pyy($taxonomy_tags_series_tag, $current='', $prefix = '') {
    foreach($taxonomy_tags_series_tag as $taxonomy_tags_pyy) {
        $selected = '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_pyy->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_pyy->tag_name.'</option>';
        if( isset( $taxonomy_tags_pyy->children ) && (count( $taxonomy_tags_pyy->children ) > 0 ) ) {
            taxonomy_tags_pyy( $taxonomy_tags_pyy->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_pyy( $taxonomy_tags_series_tag  );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="series_active" id="tv_series_series_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "tv_series"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#tv_series-tab">Series</a></li><li><a href="<?php echo site_url("tv_series_genre") . "?series_id=" . $tv_series->series_id; ?>">Genre</a></li><li><a href="<?php echo site_url("tv_series_tags") . "?series_id=" . $tv_series->series_id; ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Series</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="series_id" id="tv_series_series_id" value="<?php echo $tv_series->series_id; ?>" />

<div class="form-group"><label for="tv_series_series_title">Title</label><input type="text" name="series_title" id="tv_series_series_title" class="form-control  text " placeholder="Title" value="<?php echo $tv_series->series_title; ?>" /></div>

<div class="form-group"><label for="tv_series_series_slug">Slug</label><input type="text" name="series_slug" id="tv_series_series_slug" class="form-control  text " placeholder="Slug" value="<?php echo $tv_series->series_slug; ?>" /></div>

<div class="form-group"><label for="tv_series_series_description">Description</label><textarea name="series_description" id="tv_series_series_description" class="form-control  textarea" placeholder="Description" rows="5"><?php echo $tv_series->series_description; ?></textarea></div>

<div class="form-group"><label for="tv_series_series_year">Year</label><input type="text" name="series_year" id="tv_series_series_year" class="form-control  text " placeholder="Year" value="<?php echo $tv_series->series_year; ?>" /></div>

<div class="form-group"><label for="tv_series_series_lang">Language</label><input type="text" name="series_lang" id="tv_series_series_lang" class="form-control  text " placeholder="Language" value="<?php echo $tv_series->series_lang; ?>" /></div>

<div class="form-group"><label for="tv_series_series_poster">Picture</label><input type="text" name="series_poster" id="tv_series_series_poster" class="form-control  text " placeholder="Picture" value="<?php echo $tv_series->series_poster; ?>" /></div>

<div class="form-group"><label for="tv_series_series_tag">Tag</label><select name="series_tag" id="tv_series_series_tag" class="selectpicker form-control " placeholder="Tag" data-live-search="true"><option value="">- - Select Tag - -</option><?php 
function taxonomy_tags_sQR($taxonomy_tags_series_tag, $current='', $prefix = '') {
    foreach($taxonomy_tags_series_tag as $taxonomy_tags_sQR) {
        $selected = ($current->series_tag == $taxonomy_tags_sQR->tag_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_sQR->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_sQR->tag_name.'</option>';
        if( isset( $taxonomy_tags_sQR->children ) && (count( $taxonomy_tags_sQR->children ) > 0 ) ) {
            taxonomy_tags_sQR( $taxonomy_tags_sQR->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_sQR( $taxonomy_tags_series_tag , $tv_series );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($tv_series->series_active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="series_active" id="tv_series_series_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "tv_series"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
