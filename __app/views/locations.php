<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_locations')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "locations/add"); ?>" class="btn btn-default btn-sm pull-right">Add Location</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Parent Location</th>
            
<?php  if(  $this->session->userdata('controller_locations')->can_edit || $this->session->userdata('controller_locations')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($locations as $locations_list) { ?>
        <tr class="<?php echo ($locations_list->active) ? '' : 'danger'; ?>">
            <td><?php echo $locations_list->name; ?></td>
            <td><a href="<?php echo site_url('locations/index/parent_id/' . $locations_list->parent_id ); ?>"><?php echo $locations_list->parent_name; ?></a></td>
 
<?php  if(  $this->session->userdata('controller_locations')->can_edit || $this->session->userdata('controller_locations')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_locations')->can_edit   ) { ?>
<a href="<?php echo site_url( 'locations/edit/' . $locations_list->id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_locations')->can_delete   ) { ?>
<a href="<?php echo site_url( 'locations/delete/' . $locations_list->id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Location</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="locations_name">Name</label><input type="text" name="name" id="locations_name" class="form-control  text " placeholder="Name" value="<?php echo ($this->input->post('name')) ? $this->input->post('name') : ''; ?>" /></div>

<div class="form-group"><label for="locations_parent_id">Parent Location</label><select name="parent_id" id="locations_parent_id" class="selectpicker form-control " placeholder="Parent Location" data-live-search="true"><option value="">- - Select Parent Location - -</option><?php 
function locations_byH($locations_parent_id, $current='', $prefix = '') {
    foreach($locations_parent_id as $locations_byH) {
        $selected = '';
        echo '<option '.$selected.' value="'.$locations_byH->id.'">'.  $prefix . ' ' . $locations_byH->name.'</option>';
        if( isset( $locations_byH->children ) && (count( $locations_byH->children ) > 0 ) ) {
            locations_byH( $locations_byH->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
locations_byH( $locations_parent_id  );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="active" id="locations_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "locations"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#locations-tab">Locations</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Locations</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="locations_id" value="<?php echo $locations->id; ?>" />

<div class="form-group"><label for="locations_name">Name</label><input type="text" name="name" id="locations_name" class="form-control  text " placeholder="Name" value="<?php echo $locations->name; ?>" /></div>

<div class="form-group"><label for="locations_slug">Slug</label><input type="text" name="slug" id="locations_slug" class="form-control  text " placeholder="Slug" value="<?php echo $locations->slug; ?>" /></div>

<div class="form-group"><label for="locations_parent_id">Parent Location</label><select name="parent_id" id="locations_parent_id" class="selectpicker form-control " placeholder="Parent Location" data-live-search="true"><option value="">- - Select Parent Location - -</option><?php 
function locations_lSb($locations_parent_id, $current='', $prefix = '') {
    foreach($locations_parent_id as $locations_lSb) {
        $selected = ($current->parent_id == $locations_lSb->id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$locations_lSb->id.'">'.  $prefix . ' ' . $locations_lSb->name.'</option>';
        if( isset( $locations_lSb->children ) && (count( $locations_lSb->children ) > 0 ) ) {
            locations_lSb( $locations_lSb->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
locations_lSb( $locations_parent_id , $locations );
?></select></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($locations->active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="active" id="locations_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "locations"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
