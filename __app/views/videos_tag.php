<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('videos/edit/' . $this->input->get($filter_key) ); ?>">Videos</a></li><li class="active"><a href="<?php echo site_url("videos_tag") . "?video_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_videos_tag')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "videos_tag/add") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-default btn-sm pull-right">Add Tag</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Tag Name</th>
            
<?php  if(  $this->session->userdata('controller_videos_tag')->can_edit || $this->session->userdata('controller_videos_tag')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($videos_tag as $videos_tag_list) { ?>
        <tr class="">
            <td><?php echo $videos_tag_list->tag_name; ?></td>
 
<?php  if(  $this->session->userdata('controller_videos_tag')->can_edit || $this->session->userdata('controller_videos_tag')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_videos_tag')->can_edit   ) { ?>
<a href="<?php echo site_url( 'videos_tag/edit/' . $videos_tag_list->vt_id) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_videos_tag')->can_delete   ) { ?>
<a href="<?php echo site_url( 'videos_tag/delete/' . $videos_tag_list->vt_id ) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Tag</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="video_id" id="videos_tag_video_id" value="<?php echo $this->input->get('video_id'); ?>" />

<div class="form-group"><label for="videos_tag_tag_id">Tag Name</label><select name="tag_id" id="videos_tag_tag_id" class="selectpicker form-control " placeholder="Tag Name" data-live-search="true"><option value="">- - Select Tag Name - -</option><?php 
function taxonomy_tags_aFs($taxonomy_tags_tag_id, $current='', $prefix = '') {
    foreach($taxonomy_tags_tag_id as $taxonomy_tags_aFs) {
        $selected = '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_aFs->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_aFs->tag_name.'</option>';
        if( isset( $taxonomy_tags_aFs->children ) && (count( $taxonomy_tags_aFs->children ) > 0 ) ) {
            taxonomy_tags_aFs( $taxonomy_tags_aFs->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_aFs( $taxonomy_tags_tag_id  );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "videos_tag") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('videos/edit/' . $this->input->get($filter_key) ); ?>">Videos</a></li><li class="active"><a href="<?php echo site_url("videos_tag") . "?video_id=" . $this->input->get($filter_key); ?>">Tags</a></li>
</ul>
<br>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Tags</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="vt_id" id="videos_tag_vt_id" value="<?php echo $videos_tag->vt_id; ?>" />

<input type="hidden" name="video_id" id="videos_tag_video_id" value="<?php echo $this->input->get('video_id'); ?>" />

<div class="form-group"><label for="videos_tag_tag_id">Tag Name</label><select name="tag_id" id="videos_tag_tag_id" class="selectpicker form-control " placeholder="Tag Name" data-live-search="true"><option value="">- - Select Tag Name - -</option><?php 
function taxonomy_tags_ZtL($taxonomy_tags_tag_id, $current='', $prefix = '') {
    foreach($taxonomy_tags_tag_id as $taxonomy_tags_ZtL) {
        $selected = ($current->tag_id == $taxonomy_tags_ZtL->tag_id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$taxonomy_tags_ZtL->tag_id.'">'.  $prefix . ' ' . $taxonomy_tags_ZtL->tag_name.'</option>';
        if( isset( $taxonomy_tags_ZtL->children ) && (count( $taxonomy_tags_ZtL->children ) > 0 ) ) {
            taxonomy_tags_ZtL( $taxonomy_tags_ZtL->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
taxonomy_tags_ZtL( $taxonomy_tags_tag_id , $videos_tag );
?></select></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "videos_tag") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
