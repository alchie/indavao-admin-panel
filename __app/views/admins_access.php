<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('admins/edit/' . $this->input->get($filter_key) ); ?>">Admins</a></li><li class="active"><a href="<?php echo site_url("admins_access") . "?admin_id=" . $this->input->get($filter_key); ?>">Controller Access</a></li>
</ul>
<br>

<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_admins_access')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "admins_access/add") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-default btn-sm pull-right">Add Controller Access</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Controller</th>
            <th>Can Add</th>
            <th>Can Edit</th>
            <th>Can Delete</th>
            
<?php  if(  $this->session->userdata('controller_admins_access')->can_edit || $this->session->userdata('controller_admins_access')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($admins_access as $admins_access_list) { ?>
        <tr class="">
            <td><?php echo $admins_access_list->controller; ?></td>
            <td><?php echo $admins_access_list->add; ?></td>
            <td><?php echo $admins_access_list->edit; ?></td>
            <td><?php echo $admins_access_list->delete; ?></td>
 
<?php  if(  $this->session->userdata('controller_admins_access')->can_edit || $this->session->userdata('controller_admins_access')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_admins_access')->can_edit   ) { ?>
<a href="<?php echo site_url( 'admins_access/edit/' . $admins_access_list->id) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_admins_access')->can_delete   ) { ?>
<a href="<?php echo site_url( 'admins_access/delete/' . $admins_access_list->id ) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Controller Access</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="admin_id" id="admins_access_admin_id" value="<?php echo $this->input->get('admin_id'); ?>" />

<div class="form-group"><label for="admins_access_controller">Controller</label><select name="controller" id="admins_access_controller" class="selectpicker form-control " placeholder="Controller" data-live-search="true"><option value="">- - Select Controller - -</option><option  value="admins">admins</option><option  value="admins_access">admins_access</option><option  value="locations">locations</option><option  value="movies">movies</option><option  value="movies_genre">movies_genre</option><option  value="movies_tags">movies_tags</option><option  value="multisites">multisites</option><option  value="multisites_access">multisites_access</option><option  value="realestate_modelhouse">realestate_modelhouse</option><option  value="realestate_projects">realestate_projects</option><option  value="realestate_projects_pictures">realestate_projects_pictures</option><option  value="taxonomy_genre">taxonomy_genre</option><option  value="taxonomy_industry">taxonomy_industry</option><option  value="taxonomy_tags">taxonomy_tags</option><option  value="tv_series">tv_series</option><option  value="tv_series_genre">tv_series_genre</option><option  value="tv_series_tags">tv_series_tags</option><option  value="users">users</option><option  value="users_network">users_network</option><option  value="users_points">users_points</option><option  value="users_profile">users_profile</option><option  value="users_sessions">users_sessions</option><option  value="users_social_network">users_social_network</option><option  value="users_stats">users_stats</option><option  value="videos">videos</option><option  value="videos_tag">videos_tag</option></select></div>

<div class="form-group"><strong>Can Add</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="add" id="admins_access_add" class="">Enabled</label></div></div>

<div class="form-group"><strong>Can Edit</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="edit" id="admins_access_edit" class="">Enabled</label></div></div>

<div class="form-group"><strong>Can Delete</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="delete" id="admins_access_delete" class="">Enabled</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "admins_access") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('admins/edit/' . $this->input->get($filter_key) ); ?>">Admins</a></li><li class="active"><a href="<?php echo site_url("admins_access") . "?admin_id=" . $this->input->get($filter_key); ?>">Controller Access</a></li>
</ul>
<br>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Controller Access</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="admins_access_id" value="<?php echo $admins_access->id; ?>" />

<input type="hidden" name="admin_id" id="admins_access_admin_id" value="<?php echo $this->input->get('admin_id'); ?>" />

<div class="form-group"><label for="admins_access_controller">Controller</label><select name="controller" id="admins_access_controller" class="selectpicker form-control " placeholder="Controller" data-live-search="true"><option value="">- - Select Controller - -</option><option <?php echo ($admins_access->controller == 'admins') ? 'SELECTED' : ''; ?> value="admins">admins</option><option <?php echo ($admins_access->controller == 'admins_access') ? 'SELECTED' : ''; ?> value="admins_access">admins_access</option><option <?php echo ($admins_access->controller == 'locations') ? 'SELECTED' : ''; ?> value="locations">locations</option><option <?php echo ($admins_access->controller == 'movies') ? 'SELECTED' : ''; ?> value="movies">movies</option><option <?php echo ($admins_access->controller == 'movies_genre') ? 'SELECTED' : ''; ?> value="movies_genre">movies_genre</option><option <?php echo ($admins_access->controller == 'movies_tags') ? 'SELECTED' : ''; ?> value="movies_tags">movies_tags</option><option <?php echo ($admins_access->controller == 'multisites') ? 'SELECTED' : ''; ?> value="multisites">multisites</option><option <?php echo ($admins_access->controller == 'multisites_access') ? 'SELECTED' : ''; ?> value="multisites_access">multisites_access</option><option <?php echo ($admins_access->controller == 'realestate_modelhouse') ? 'SELECTED' : ''; ?> value="realestate_modelhouse">realestate_modelhouse</option><option <?php echo ($admins_access->controller == 'realestate_projects') ? 'SELECTED' : ''; ?> value="realestate_projects">realestate_projects</option><option <?php echo ($admins_access->controller == 'realestate_projects_pictures') ? 'SELECTED' : ''; ?> value="realestate_projects_pictures">realestate_projects_pictures</option><option <?php echo ($admins_access->controller == 'taxonomy_genre') ? 'SELECTED' : ''; ?> value="taxonomy_genre">taxonomy_genre</option><option <?php echo ($admins_access->controller == 'taxonomy_industry') ? 'SELECTED' : ''; ?> value="taxonomy_industry">taxonomy_industry</option><option <?php echo ($admins_access->controller == 'taxonomy_tags') ? 'SELECTED' : ''; ?> value="taxonomy_tags">taxonomy_tags</option><option <?php echo ($admins_access->controller == 'tv_series') ? 'SELECTED' : ''; ?> value="tv_series">tv_series</option><option <?php echo ($admins_access->controller == 'tv_series_genre') ? 'SELECTED' : ''; ?> value="tv_series_genre">tv_series_genre</option><option <?php echo ($admins_access->controller == 'tv_series_tags') ? 'SELECTED' : ''; ?> value="tv_series_tags">tv_series_tags</option><option <?php echo ($admins_access->controller == 'users') ? 'SELECTED' : ''; ?> value="users">users</option><option <?php echo ($admins_access->controller == 'users_network') ? 'SELECTED' : ''; ?> value="users_network">users_network</option><option <?php echo ($admins_access->controller == 'users_points') ? 'SELECTED' : ''; ?> value="users_points">users_points</option><option <?php echo ($admins_access->controller == 'users_profile') ? 'SELECTED' : ''; ?> value="users_profile">users_profile</option><option <?php echo ($admins_access->controller == 'users_sessions') ? 'SELECTED' : ''; ?> value="users_sessions">users_sessions</option><option <?php echo ($admins_access->controller == 'users_social_network') ? 'SELECTED' : ''; ?> value="users_social_network">users_social_network</option><option <?php echo ($admins_access->controller == 'users_stats') ? 'SELECTED' : ''; ?> value="users_stats">users_stats</option><option <?php echo ($admins_access->controller == 'videos') ? 'SELECTED' : ''; ?> value="videos">videos</option><option <?php echo ($admins_access->controller == 'videos_tag') ? 'SELECTED' : ''; ?> value="videos_tag">videos_tag</option></select></div>

<div class="form-group"><strong>Can Add</strong><div class="checkbox"><label><input <?php echo ($admins_access->add == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="add" id="admins_access_add" class="">Enabled</label></div></div>

<div class="form-group"><strong>Can Edit</strong><div class="checkbox"><label><input <?php echo ($admins_access->edit == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="edit" id="admins_access_edit" class="">Enabled</label></div></div>

<div class="form-group"><strong>Can Delete</strong><div class="checkbox"><label><input <?php echo ($admins_access->delete == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="delete" id="admins_access_delete" class="">Enabled</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "admins_access") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
