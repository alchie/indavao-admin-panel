<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_multisites')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "multisites/add"); ?>" class="btn btn-default btn-sm pull-right">Add Website</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Site ID</th>
            <th>Title</th>
            <th>URL</th>
            
<?php  if(  $this->session->userdata('controller_multisites')->can_edit || $this->session->userdata('controller_multisites')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($multisites as $multisites_list) { ?>
        <tr class="">
            <td><?php echo $multisites_list->site_name; ?></td>
            <td><?php echo $multisites_list->title; ?></td>
            <td><?php echo $multisites_list->url; ?></td>
 
<?php  if(  $this->session->userdata('controller_multisites')->can_edit || $this->session->userdata('controller_multisites')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_multisites')->can_edit   ) { ?>
<a href="<?php echo site_url( 'multisites/edit/' . $multisites_list->site_id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_multisites')->can_delete   ) { ?>
<a href="<?php echo site_url( 'multisites/delete/' . $multisites_list->site_id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Website</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="multisites_site_name">Site ID</label><input type="text" name="site_name" id="multisites_site_name" class="form-control  text " placeholder="Site ID" value="<?php echo ($this->input->post('site_name')) ? $this->input->post('site_name') : ''; ?>" /></div>

<div class="form-group"><label for="multisites_title">Title</label><input type="text" name="title" id="multisites_title" class="form-control  text " placeholder="Title" value="<?php echo ($this->input->post('title')) ? $this->input->post('title') : ''; ?>" /></div>

<div class="form-group"><label for="multisites_description">Description</label><input type="text" name="description" id="multisites_description" class="form-control  text " placeholder="Description" value="<?php echo ($this->input->post('description')) ? $this->input->post('description') : ''; ?>" /></div>

<div class="form-group"><label for="multisites_url">URL</label><input type="text" name="url" id="multisites_url" class="form-control  text " placeholder="URL" value="<?php echo ($this->input->post('url')) ? $this->input->post('url') : ''; ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "multisites"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#multisites-tab">Websites</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Websites</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="site_id" id="multisites_site_id" value="<?php echo $multisites->site_id; ?>" />

<div class="form-group"><label for="multisites_site_name">Site ID</label><input type="text" name="site_name" id="multisites_site_name" class="form-control  text " placeholder="Site ID" value="<?php echo $multisites->site_name; ?>" /></div>

<div class="form-group"><label for="multisites_title">Title</label><input type="text" name="title" id="multisites_title" class="form-control  text " placeholder="Title" value="<?php echo $multisites->title; ?>" /></div>

<div class="form-group"><label for="multisites_description">Description</label><input type="text" name="description" id="multisites_description" class="form-control  text " placeholder="Description" value="<?php echo $multisites->description; ?>" /></div>

<div class="form-group"><label for="multisites_url">URL</label><input type="text" name="url" id="multisites_url" class="form-control  text " placeholder="URL" value="<?php echo $multisites->url; ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "multisites"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
