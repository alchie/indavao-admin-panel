<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_taxonomy_genre')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "taxonomy_genre/add"); ?>" class="btn btn-default btn-sm pull-right">Add Genre</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Name</th>
            
<?php  if(  $this->session->userdata('controller_taxonomy_genre')->can_edit || $this->session->userdata('controller_taxonomy_genre')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($taxonomy_genre as $taxonomy_genre_list) { ?>
        <tr class="<?php echo ($taxonomy_genre_list->genre_active) ? '' : 'danger'; ?>">
            <td><?php echo $taxonomy_genre_list->genre_name; ?></td>
 
<?php  if(  $this->session->userdata('controller_taxonomy_genre')->can_edit || $this->session->userdata('controller_taxonomy_genre')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_taxonomy_genre')->can_edit   ) { ?>
<a href="<?php echo site_url( 'taxonomy_genre/edit/' . $taxonomy_genre_list->genre_id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_taxonomy_genre')->can_delete   ) { ?>
<a href="<?php echo site_url( 'taxonomy_genre/delete/' . $taxonomy_genre_list->genre_id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Genre</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="taxonomy_genre_genre_name">Name</label><input type="text" name="genre_name" id="taxonomy_genre_genre_name" class="form-control  text " placeholder="Name" value="<?php echo ($this->input->post('genre_name')) ? $this->input->post('genre_name') : ''; ?>" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="genre_active" id="taxonomy_genre_genre_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "taxonomy_genre"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#taxonomy_genre-tab">Genre</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Genre</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="genre_id" id="taxonomy_genre_genre_id" value="<?php echo $taxonomy_genre->genre_id; ?>" />

<div class="form-group"><label for="taxonomy_genre_genre_name">Name</label><input type="text" name="genre_name" id="taxonomy_genre_genre_name" class="form-control  text " placeholder="Name" value="<?php echo $taxonomy_genre->genre_name; ?>" /></div>

<div class="form-group"><label for="taxonomy_genre_genre_slug">Slug</label><input type="text" name="genre_slug" id="taxonomy_genre_genre_slug" class="form-control  text " placeholder="Slug" value="<?php echo $taxonomy_genre->genre_slug; ?>" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($taxonomy_genre->genre_active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="genre_active" id="taxonomy_genre_genre_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "taxonomy_genre"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
