
    <div id="wrapper">

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>">Admin Panel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li>Welcome <?php echo  $active_session['admin_username']; ?>! &middot; <small><strong>Last Login:</strong> <?php echo  $active_session['last_login']; ?> (<?php echo  $active_session['last_login_ip']; ?>)</small> &nbsp; </li>
                <li><a href="<?php echo site_url('change_password'); ?>"><i class="fa fa-sign-out fa-fw"></i> Change Password</a></li>
                 <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">

<li>
    <a id="menu-dashboard" href="<?php echo site_url('dashboard'); ?>" title="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li><?php  if($this->session->userdata('controller_realestate_projects')) { ?>
<li <?php echo ($main_page == 'ads') ? 'class="active"' : ''; ?>>
    <a id="menu-ads" href="#" title="ads" class=""><i class="fa fa-bars fa-fw"></i> Ads<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"><?php  if(  $this->session->userdata('controller_realestate_projects')   ) { ?>
<li <?php echo ($sub_page == 'realestate_projects') ? 'class="active"' : ''; ?>>
    <a id="menu-realestate_projects" href="<?php echo site_url('realestate_projects'); ?>" title="Real Estate Projects" class=""><i class="fa fa-arrow-right fa-fw"></i> Real Estate Projects</a>
</li>

<?php } ?></ul>
</li><?php } ?><?php  if($this->session->userdata('controller_movies')||$this->session->userdata('controller_tv_series')||$this->session->userdata('controller_videos')) { ?>
<li <?php echo ($main_page == 'multimedia') ? 'class="active"' : ''; ?>>
    <a id="menu-multimedia" href="#" title="multimedia" class=""><i class="fa fa-bars fa-fw"></i> Multimedia<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"><?php  if(  $this->session->userdata('controller_movies')   ) { ?>
<li <?php echo ($sub_page == 'movies') ? 'class="active"' : ''; ?>>
    <a id="menu-movies" href="<?php echo site_url('movies'); ?>" title="Movies" class=""><i class="fa fa-arrow-right fa-fw"></i> Movies</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_tv_series')   ) { ?>
<li <?php echo ($sub_page == 'tv_series') ? 'class="active"' : ''; ?>>
    <a id="menu-tv_series" href="<?php echo site_url('tv_series'); ?>" title="TV Series" class=""><i class="fa fa-arrow-right fa-fw"></i> TV Series</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_videos')   ) { ?>
<li <?php echo ($sub_page == 'videos') ? 'class="active"' : ''; ?>>
    <a id="menu-videos" href="<?php echo site_url('videos'); ?>" title="Videos" class=""><i class="fa fa-arrow-right fa-fw"></i> Videos</a>
</li>

<?php } ?></ul>
</li><?php } ?><?php  if($this->session->userdata('controller_locations')||$this->session->userdata('controller_taxonomy_genre')||$this->session->userdata('controller_taxonomy_industry')||$this->session->userdata('controller_taxonomy_tags')) { ?>
<li <?php echo ($main_page == 'taxonomy') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomy" href="#" title="taxonomy" class=""><i class="fa fa-bars fa-fw"></i> Taxonomy<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"><?php  if(  $this->session->userdata('controller_locations')   ) { ?>
<li <?php echo ($sub_page == 'locations') ? 'class="active"' : ''; ?>>
    <a id="menu-locations" href="<?php echo site_url('locations'); ?>" title="Locations" class=""><i class="fa fa-arrow-right fa-fw"></i> Locations</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_taxonomy_genre')   ) { ?>
<li <?php echo ($sub_page == 'taxonomy_genre') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomy_genre" href="<?php echo site_url('taxonomy_genre'); ?>" title="Genre" class=""><i class="fa fa-arrow-right fa-fw"></i> Genre</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_taxonomy_industry')   ) { ?>
<li <?php echo ($sub_page == 'taxonomy_industry') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomy_industry" href="<?php echo site_url('taxonomy_industry'); ?>" title="Industries" class=""><i class="fa fa-arrow-right fa-fw"></i> Industries</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_taxonomy_tags')   ) { ?>
<li <?php echo ($sub_page == 'taxonomy_tags') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomy_tags" href="<?php echo site_url('taxonomy_tags'); ?>" title="Tags" class=""><i class="fa fa-arrow-right fa-fw"></i> Tags</a>
</li>

<?php } ?></ul>
</li><?php } ?><?php  if($this->session->userdata('controller_admins')||$this->session->userdata('controller_users')) { ?>
<li <?php echo ($main_page == 'accounts') ? 'class="active"' : ''; ?>>
    <a id="menu-accounts" href="#" title="accounts" class=""><i class="fa fa-bars fa-fw"></i> Accounts<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"><?php  if(  $this->session->userdata('controller_admins')   ) { ?>
<li <?php echo ($sub_page == 'admins') ? 'class="active"' : ''; ?>>
    <a id="menu-admins" href="<?php echo site_url('admins'); ?>" title="Admins" class=""><i class="fa fa-arrow-right fa-fw"></i> Admins</a>
</li>

<?php } ?><?php  if(  $this->session->userdata('controller_users')   ) { ?>
<li <?php echo ($sub_page == 'users') ? 'class="active"' : ''; ?>>
    <a id="menu-users" href="<?php echo site_url('users'); ?>" title="Users" class=""><i class="fa fa-arrow-right fa-fw"></i> Users</a>
</li>

<?php } ?></ul>
</li><?php } ?><?php  if($this->session->userdata('controller_multisites')) { ?>
<li <?php echo ($main_page == 'settings') ? 'class="active"' : ''; ?>>
    <a id="menu-settings" href="#" title="settings" class=""><i class="fa fa-bars fa-fw"></i> Settings<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"><?php  if(  $this->session->userdata('controller_multisites')   ) { ?>
<li <?php echo ($sub_page == 'multisites') ? 'class="active"' : ''; ?>>
    <a id="menu-multisites" href="<?php echo site_url('multisites'); ?>" title="Websites" class=""><i class="fa fa-arrow-right fa-fw"></i> Websites</a>
</li>

<?php } ?></ul>
</li><?php } ?>

                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">

