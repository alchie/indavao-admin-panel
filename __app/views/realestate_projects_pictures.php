<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('realestate_projects/edit/' . $this->input->get($filter_key) ); ?>">Real Estate Projects</a></li><li ><a href="<?php echo site_url("realestate_modelhouse") . "?project_id=" . $this->input->get($filter_key); ?>">Model Houses</a></li><li class="active"><a href="<?php echo site_url("realestate_projects_pictures") . "?project_id=" . $this->input->get($filter_key); ?>">Pictures</a></li>
</ul>
<br>

<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_realestate_projects_pictures')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "realestate_projects_pictures/add") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-default btn-sm pull-right">Add Picture</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Project Name</th>
            <th>Image URL</th>
            
<?php  if(  $this->session->userdata('controller_realestate_projects_pictures')->can_edit || $this->session->userdata('controller_realestate_projects_pictures')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($realestate_projects_pictures as $realestate_projects_pictures_list) { ?>
        <tr class="">
            <td><a href="<?php echo site_url('realestate_projects_pictures/index/project_id/' . $realestate_projects_pictures_list->project_id ); ?>"><?php echo $realestate_projects_pictures_list->project_name; ?></a></td>
            <td><?php echo $realestate_projects_pictures_list->url; ?></td>
 
<?php  if(  $this->session->userdata('controller_realestate_projects_pictures')->can_edit || $this->session->userdata('controller_realestate_projects_pictures')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_realestate_projects_pictures')->can_edit   ) { ?>
<a href="<?php echo site_url( 'realestate_projects_pictures/edit/' . $realestate_projects_pictures_list->id) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_realestate_projects_pictures')->can_delete   ) { ?>
<a href="<?php echo site_url( 'realestate_projects_pictures/delete/' . $realestate_projects_pictures_list->id ) . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Picture</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="project_id" id="realestate_projects_pictures_project_id" value="<?php echo $this->input->get('project_id'); ?>" />

<div class="form-group"><label for="realestate_projects_pictures_url">Image URL</label><input type="text" name="url" id="realestate_projects_pictures_url" class="form-control  text " placeholder="Image URL" value="<?php echo ($this->input->post('url')) ? $this->input->post('url') : ''; ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "realestate_projects_pictures") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li><a href="<?php echo site_url('realestate_projects/edit/' . $this->input->get($filter_key) ); ?>">Real Estate Projects</a></li><li ><a href="<?php echo site_url("realestate_modelhouse") . "?project_id=" . $this->input->get($filter_key); ?>">Model Houses</a></li><li class="active"><a href="<?php echo site_url("realestate_projects_pictures") . "?project_id=" . $this->input->get($filter_key); ?>">Pictures</a></li>
</ul>
<br>

<form action="<?php echo current_url() . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Pictures</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="realestate_projects_pictures_id" value="<?php echo $realestate_projects_pictures->id; ?>" />

<input type="hidden" name="project_id" id="realestate_projects_pictures_project_id" value="<?php echo $this->input->get('project_id'); ?>" />

<div class="form-group"><label for="realestate_projects_pictures_url">Image URL</label><input type="text" name="url" id="realestate_projects_pictures_url" class="form-control  text " placeholder="Image URL" value="<?php echo $realestate_projects_pictures->url; ?>" /></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "realestate_projects_pictures") . "?" .$filter_key . "=" . $this->input->get($filter_key); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
