<div class="row">
                <div class="col-lg-12">
                
                    <h1 class="page-header"><?php echo $page_title; ?></h1>
                    

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<?php if ($action == NULL) : ?>



<div class="panel panel-default">
<?php  if(  $this->session->userdata('controller_realestate_projects')->can_add   ) { ?>
                        <div class="panel-heading">
                             <a href="<?php echo site_url( "realestate_projects/add"); ?>" class="btn btn-default btn-sm pull-right">Add Project</a>
                             <div class="clearfix"></div>
                        </div>
<?php } ?>
                        <!-- /.panel-heading -->
<div class="panel-body">
     
<div class="table-responsive">


<table id="dataTables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Location</th>
            
<?php  if(  $this->session->userdata('controller_realestate_projects')->can_edit || $this->session->userdata('controller_realestate_projects')->can_delete  ) { ?>
<td width="100">Actions</td>
<?php } ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach($realestate_projects as $realestate_projects_list) { ?>
        <tr class="<?php echo ($realestate_projects_list->active) ? '' : 'danger'; ?>">
            <td><?php echo $realestate_projects_list->name; ?></td>
            <td><a href="<?php echo site_url('realestate_projects/index/location/' . $realestate_projects_list->location ); ?>"><?php echo $realestate_projects_list->location_name; ?></a></td>
 
<?php  if(  $this->session->userdata('controller_realestate_projects')->can_edit || $this->session->userdata('controller_realestate_projects')->can_delete  ) { ?>
<td>
<?php  if(  $this->session->userdata('controller_realestate_projects')->can_edit   ) { ?>
<a href="<?php echo site_url( 'realestate_projects/edit/' . $realestate_projects_list->id); ?>" class="btn btn-success btn-xs">Edit</a> 
<?php } ?>
&middot; 
<?php  if(  $this->session->userdata('controller_realestate_projects')->can_delete   ) { ?>
<a href="<?php echo site_url( 'realestate_projects/delete/' . $realestate_projects_list->id ); ?>" class="btn btn-danger btn-xs btn-delete">Delete</a>
<?php } ?>
</td>
<?php } ?>
        </tr>   
        <?php } ?>
    </tbody>
</table>
</div>

</div>

</div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php elseif($action == 'add') : ?>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title">Add Project</h3>
                             <div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group"><label for="realestate_projects_name">Name</label><input type="text" name="name" id="realestate_projects_name" class="form-control  text " placeholder="Name" value="<?php echo ($this->input->post('name')) ? $this->input->post('name') : ''; ?>" /></div>

<div class="form-group"><label for="realestate_projects_address">Address</label><input type="text" name="address" id="realestate_projects_address" class="form-control  text " placeholder="Address" value="<?php echo ($this->input->post('address')) ? $this->input->post('address') : ''; ?>" /></div>

<div class="form-group"><label for="realestate_projects_location">Location</label><select name="location" id="realestate_projects_location" class="selectpicker form-control " placeholder="Location" data-live-search="true"><option value="">- - Select Location - -</option><?php 
function locations_LRW($locations_location, $current='', $prefix = '') {
    foreach($locations_location as $locations_LRW) {
        $selected = '';
        echo '<option '.$selected.' value="'.$locations_LRW->id.'">'.  $prefix . ' ' . $locations_LRW->name.'</option>';
        if( isset( $locations_LRW->children ) && (count( $locations_LRW->children ) > 0 ) ) {
            locations_LRW( $locations_LRW->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
locations_LRW( $locations_location  );
?></select></div>

<div class="form-group"><label for="realestate_projects_description">Description</label><textarea name="description" id="realestate_projects_description" class="form-control  textarea" placeholder="Description" rows="5"></textarea></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input CHECKED type="checkbox" value="1"name="active" id="realestate_projects_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "realestate_projects"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>

</div> <!-- .panel -->

<?php elseif($action == 'edit') : ?>
 
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#realestate_projects-tab">Real Estate Projects</a></li><li><a href="<?php echo site_url("realestate_modelhouse") . "?project_id=" . $realestate_projects->id; ?>">Model Houses</a></li><li><a href="<?php echo site_url("realestate_projects_pictures") . "?project_id=" . $realestate_projects->id; ?>">Pictures</a></li>
</ul>
<br>

<form action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
<div class="panel panel-default">
                        <!--<div class="panel-heading">
                             <h3 class="panel-title">Edit Real Estate Projects</h3>
                             <div class="clearfix"></div>
                        </div>-->
                        <!-- /.panel-heading -->
<div class="panel-body">

<input type="hidden" name="id" id="realestate_projects_id" value="<?php echo $realestate_projects->id; ?>" />

<div class="form-group"><label for="realestate_projects_name">Name</label><input type="text" name="name" id="realestate_projects_name" class="form-control  text " placeholder="Name" value="<?php echo $realestate_projects->name; ?>" /></div>

<div class="form-group"><label for="realestate_projects_address">Address</label><input type="text" name="address" id="realestate_projects_address" class="form-control  text " placeholder="Address" value="<?php echo $realestate_projects->address; ?>" /></div>

<div class="form-group"><label for="realestate_projects_location">Location</label><select name="location" id="realestate_projects_location" class="selectpicker form-control " placeholder="Location" data-live-search="true"><option value="">- - Select Location - -</option><?php 
function locations_CgR($locations_location, $current='', $prefix = '') {
    foreach($locations_location as $locations_CgR) {
        $selected = ($current->location == $locations_CgR->id) ? 'SELECTED' : '';
        echo '<option '.$selected.' value="'.$locations_CgR->id.'">'.  $prefix . ' ' . $locations_CgR->name.'</option>';
        if( isset( $locations_CgR->children ) && (count( $locations_CgR->children ) > 0 ) ) {
            locations_CgR( $locations_CgR->children, $current, $prefix . ' - - - - - ' );
        }
    }
}
locations_CgR( $locations_location , $realestate_projects );
?></select></div>

<div class="form-group"><label for="realestate_projects_description">Description</label><textarea name="description" id="realestate_projects_description" class="form-control  textarea" placeholder="Description" rows="5"><?php echo $realestate_projects->description; ?></textarea></div>

<div class="form-group"><label for="realestate_projects_slug">Slug</label><input type="text" name="slug" id="realestate_projects_slug" class="form-control  text " placeholder="Slug" value="<?php echo $realestate_projects->slug; ?>" /></div>

<div class="form-group"><strong>Active</strong><div class="checkbox"><label><input <?php echo ($realestate_projects->active == '1') ? 'CHECKED' : ''; ?> type="checkbox" value="1"name="active" id="realestate_projects_active" class="">Active</label></div></div>



</div> <!-- .panel-body -->

<div class="panel-footer">
<button type="submit" class="btn btn-success btn-sm">Submit</button>
<a href="<?php echo site_url( "realestate_projects"); ?>" class="btn btn-danger btn-sm">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</form>



</div> <!-- .panel -->

<?php endif; ?>
</div> <!-- /.col-lg-12 -->
</div>
