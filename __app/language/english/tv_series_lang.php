<?php

// Tv series

$lang['tv_series_series_id'] = 'Tv series Series_id';
$lang['tv_series_series_title'] = 'Tv series Series_title';
$lang['tv_series_series_slug'] = 'Tv series Series_slug';
$lang['tv_series_series_description'] = 'Tv series Series_description';
$lang['tv_series_series_year'] = 'Tv series Series_year';
$lang['tv_series_series_lang'] = 'Tv series Series_lang';
$lang['tv_series_series_poster'] = 'Tv series Series_poster';
$lang['tv_series_series_active'] = 'Tv series Series_active';
$lang['tv_series_series_tag'] = 'Tv series Series_tag';

/* End of file tv_series.php */

/* Location: ./application/controllers/tv_series.php */