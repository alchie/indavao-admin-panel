<?php

// Taxonomy tags

$lang['taxonomy_tags_tag_id'] = 'Taxonomy tags Tag_id';
$lang['taxonomy_tags_tag_name'] = 'Taxonomy tags Tag_name';
$lang['taxonomy_tags_tag_slug'] = 'Taxonomy tags Tag_slug';
$lang['taxonomy_tags_tag_active'] = 'Taxonomy tags Tag_active';

/* End of file taxonomy_tags.php */

/* Location: ./application/controllers/taxonomy_tags.php */