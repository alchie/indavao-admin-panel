<?php

// Taxonomy genre

$lang['taxonomy_genre_genre_id'] = 'Taxonomy genre Genre_id';
$lang['taxonomy_genre_genre_name'] = 'Taxonomy genre Genre_name';
$lang['taxonomy_genre_genre_slug'] = 'Taxonomy genre Genre_slug';
$lang['taxonomy_genre_genre_active'] = 'Taxonomy genre Genre_active';

/* End of file taxonomy_genre.php */

/* Location: ./application/controllers/taxonomy_genre.php */