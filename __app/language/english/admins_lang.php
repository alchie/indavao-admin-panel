<?php

// Admins

$lang['admins_id'] = 'Admins Id';
$lang['admins_email'] = 'Admins Email';
$lang['admins_password'] = 'Admins Password';
$lang['admins_username'] = 'Admins Username';
$lang['admins_active'] = 'Admins Active';
$lang['admins_date_created'] = 'Admins Date_created';
$lang['admins_last_login'] = 'Admins Last_login';
$lang['admins_last_login_ip'] = 'Admins Last_login_ip';

/* End of file admins.php */

/* Location: ./application/controllers/admins.php */