<?php

// Taxonomy industry

$lang['taxonomy_industry_id'] = 'Taxonomy industry Id';
$lang['taxonomy_industry_name'] = 'Taxonomy industry Name';
$lang['taxonomy_industry_description'] = 'Taxonomy industry Description';
$lang['taxonomy_industry_slug'] = 'Taxonomy industry Slug';
$lang['taxonomy_industry_active'] = 'Taxonomy industry Active';

/* End of file taxonomy_industry.php */

/* Location: ./application/controllers/taxonomy_industry.php */