<?php

// Admins access

$lang['admins_access_id'] = 'Admins access Id';
$lang['admins_access_admin_id'] = 'Admins access Admin_id';
$lang['admins_access_controller'] = 'Admins access Controller';
$lang['admins_access_add'] = 'Admins access Add';
$lang['admins_access_edit'] = 'Admins access Edit';
$lang['admins_access_delete'] = 'Admins access Delete';

/* End of file admins_access.php */

/* Location: ./application/controllers/admins_access.php */