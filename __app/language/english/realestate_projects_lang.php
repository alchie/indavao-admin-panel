<?php

// Realestate projects

$lang['realestate_projects_id'] = 'Realestate projects Id';
$lang['realestate_projects_name'] = 'Realestate projects Name';
$lang['realestate_projects_address'] = 'Realestate projects Address';
$lang['realestate_projects_location'] = 'Realestate projects Location';
$lang['realestate_projects_devoper_name'] = 'Realestate projects Devoper_name';
$lang['realestate_projects_developer_url'] = 'Realestate projects Developer_url';
$lang['realestate_projects_description'] = 'Realestate projects Description';
$lang['realestate_projects_map'] = 'Realestate projects Map';
$lang['realestate_projects_logo_url'] = 'Realestate projects Logo_url';
$lang['realestate_projects_slug'] = 'Realestate projects Slug';
$lang['realestate_projects_active'] = 'Realestate projects Active';

/* End of file realestate_projects.php */

/* Location: ./application/controllers/realestate_projects.php */