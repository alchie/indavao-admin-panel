<?php

// Movies

$lang['movies_movie_id'] = 'Movies Movie_id';
$lang['movies_movie_title'] = 'Movies Movie_title';
$lang['movies_movie_description'] = 'Movies Movie_description';
$lang['movies_movie_year'] = 'Movies Movie_year';
$lang['movies_movie_lang'] = 'Movies Movie_lang';
$lang['movies_movie_slug'] = 'Movies Movie_slug';
$lang['movies_movie_active'] = 'Movies Movie_active';
$lang['movies_movie_poster'] = 'Movies Movie_poster';
$lang['movies_movie_imdb'] = 'Movies Movie_imdb';
$lang['movies_movie_tag'] = 'Movies Movie_tag';

/* End of file movies.php */

/* Location: ./application/controllers/movies.php */