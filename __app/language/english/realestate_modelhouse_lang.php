<?php

// Realestate modelhouse

$lang['realestate_modelhouse_id'] = 'Realestate modelhouse Id';
$lang['realestate_modelhouse_project_id'] = 'Realestate modelhouse Project_id';
$lang['realestate_modelhouse_name'] = 'Realestate modelhouse Name';
$lang['realestate_modelhouse_slug'] = 'Realestate modelhouse Slug';
$lang['realestate_modelhouse_logo'] = 'Realestate modelhouse Logo';
$lang['realestate_modelhouse_price'] = 'Realestate modelhouse Price';

/* End of file realestate_modelhouse.php */

/* Location: ./application/controllers/realestate_modelhouse.php */