<?php

// Multisites

$lang['multisites_site_id'] = 'Multisites Site_id';
$lang['multisites_site_name'] = 'Multisites Site_name';
$lang['multisites_title'] = 'Multisites Title';
$lang['multisites_description'] = 'Multisites Description';
$lang['multisites_url'] = 'Multisites Url';

/* End of file multisites.php */

/* Location: ./application/controllers/multisites.php */