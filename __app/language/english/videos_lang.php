<?php

// Videos

$lang['videos_video_id'] = 'Videos Video_id';
$lang['videos_video_title'] = 'Videos Video_title';
$lang['videos_video_slug'] = 'Videos Video_slug';
$lang['videos_video_url'] = 'Videos Video_url';
$lang['videos_video_source'] = 'Videos Video_source';
$lang['videos_video_image'] = 'Videos Video_image';
$lang['videos_video_description'] = 'Videos Video_description';
$lang['videos_video_parent'] = 'Videos Video_parent';
$lang['videos_video_active'] = 'Videos Video_active';
$lang['videos_video_added'] = 'Videos Video_added';
$lang['videos_video_priority'] = 'Videos Video_priority';

/* End of file videos.php */

/* Location: ./application/controllers/videos.php */