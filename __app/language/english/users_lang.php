<?php

// Users

$lang['users_id'] = 'Users Id';
$lang['users_fullname'] = 'Users Fullname';
$lang['users_email'] = 'Users Email';
$lang['users_password'] = 'Users Password';
$lang['users_user_id'] = 'Users User_id';
$lang['users_username'] = 'Users Username';
$lang['users_level'] = 'Users Level';
$lang['users_active'] = 'Users Active';
$lang['users_activation_code'] = 'Users Activation_code';
$lang['users_type'] = 'Users Type';
$lang['users_date_joined'] = 'Users Date_joined';
$lang['users_ip_joined'] = 'Users Ip_joined';
$lang['users_referrer'] = 'Users Referrer';
$lang['users_last_login'] = 'Users Last_login';
$lang['users_last_login_ip'] = 'Users Last_login_ip';

/* End of file users.php */

/* Location: ./application/controllers/users.php */