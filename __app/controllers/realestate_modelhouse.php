<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_modelhouse extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_realestate_modelhouse')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('project_id') == '') {
                redirect('realestate_projects','location');
        } 
        $this->template_data->set('filter_key', 'project_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('realestate_modelhouse');
        $this->load->model( array('Realestate_modelhouse_model', 'Realestate_projects_model') );
        
        $this->template_data->set('main_page', 'ads' ); 
        $this->template_data->set('sub_page', 'realestate_projects' ); 
        $this->template_data->set('page_title', 'Model Houses' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Realestate_modelhouse_model->setFilter( 'realestate_modelhouse.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('project_id') != '') {
                $this->Realestate_modelhouse_model->setFilter( 'realestate_modelhouse.project_id', $this->input->get('project_id') );
        }
        
                $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
        
        $this->Realestate_modelhouse_model->setLimit(100);
        
		$this->Realestate_modelhouse_model->setJoin('realestate_projects as realestate_projects_eLd', 'realestate_modelhouse.project_id = realestate_projects_eLd.id');
		$this->Realestate_modelhouse_model->setSelect('realestate_modelhouse.*');
		$this->Realestate_modelhouse_model->setSelect('realestate_projects_eLd.name as project_name');


        $this->template_data->set('realestate_modelhouse', $this->Realestate_modelhouse_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('realestate_modelhouse', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Realestate_modelhouse_model->setStart($start);
	    $this->Realestate_modelhouse_model->setLimit(100);


		$this->Realestate_modelhouse_model->setJoin('realestate_projects as realestate_projects_eLd', 'realestate_modelhouse.project_id = realestate_projects_eLd.id');
		$this->Realestate_modelhouse_model->setSelect('realestate_modelhouse.*');
		$this->Realestate_modelhouse_model->setSelect('realestate_projects_eLd.name as project_name');


 
        $this->template_data->set('realestate_modelhouse', $this->Realestate_modelhouse_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Model Houses" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_modelhouse', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_realestate_modelhouse')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Realestate_projects_model->setOrder('name', 'ASC');
		$this->Realestate_projects_model->setLimit(0);
		$this->Realestate_projects_model->limitDataFields(array('id','name'));
		$this->template_data->set('realestate_projects_project_id', $this->Realestate_projects_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Model House" ); 
	    
        $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_modelhouse', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_realestate_modelhouse')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Realestate_modelhouse_model->setId($id);
	    $current_item = $this->Realestate_modelhouse_model->getById();
        $this->template_data->set('realestate_modelhouse',  $current_item);
        
        
		$this->Realestate_projects_model->setOrder('name', 'ASC');
		$this->Realestate_projects_model->setLimit(0);
		$this->Realestate_projects_model->limitDataFields(array('id','name'));
		$this->template_data->set('realestate_projects_project_id', $this->Realestate_projects_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_modelhouse', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_realestate_modelhouse')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Realestate_modelhouse_model->setId($id);
         $this->template_data->set('realestate_modelhouse', $this->Realestate_modelhouse_model->deleteById() );
         $this->output->set_header("location: " . site_url('realestate_modelhouse') . "?project_id=" . $this->input->get('project_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('project_id', 'lang:realestate_modelhouse_project_id', 'required');
			$this->form_validation->set_rules('name', 'lang:realestate_modelhouse_name', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:realestate_modelhouse_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Realestate_modelhouse_model->setId( $this->input->post('id') );
			$this->Realestate_modelhouse_model->setProjectId( $this->input->post('project_id') );
			$this->Realestate_modelhouse_model->setName( $this->input->post('name') );

			if( $action == 'edit' ) {
				$this->Realestate_modelhouse_model->setSlug( url_title($this->input->post('slug'), '-', TRUE) );
			} else {
				$this->Realestate_modelhouse_model->setSlug(  url_title($this->input->post('name'), '-', TRUE) );
			}

			$this->Realestate_modelhouse_model->setLogo( $this->input->post('logo') );
			$this->Realestate_modelhouse_model->setPrice( $this->input->post('price') );

			if( $action == 'add' ) {
				if( $this->Realestate_modelhouse_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Realestate_modelhouse_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file realestate_modelhouse.php */
/* Location: ./application/controllers/realestate_modelhouse.php */