<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('template_data');
    }
    
	public function index()
	{
	    
	    $this->template_data->set('active_session', $this->session->all_userdata() );
	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('dashboard', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function unauthorized()
	{
	    
	    $this->template_data->set('active_session', $this->session->all_userdata() );
	    $this->template_data->set('unauthorized', true );
	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('dashboard', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	    
	public function change_password()
	{
	    
	    $this->template_data->set('active_session', $this->session->all_userdata() );
	    	    
	    $this->load->library(array('form_validation') );
	    
	    $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean|md5|sha1');
	    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|md5|sha1');
	    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[new_password]|md5|sha1');
		
		if ($this->form_validation->run() == FALSE)
		{
		     if( $this->input->post() ) {
		         $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else 
		{
		    
		    $this->load->model(array('Admins_model'));
		    $this->Admins_model->setId( $this->session->userdata('admin_id'), true );
            $this->Admins_model->setPassword( $this->input->post( 'old_password' ), true );
            
		    if ( $this->Admins_model->nonempty() === TRUE ) { 
				$this->Admins_model->setPassword( $this->input->post( 'new_password' ), false, true );
				$this->Admins_model->update();
				$this->template_data->alert( 'Password Changed!', 'success');       
		    } else {
				$this->template_data->alert( 'Password Incorrect!', 'danger'); 
			}
		}
	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('change_password', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
}


