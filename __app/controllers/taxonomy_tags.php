<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taxonomy_tags extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_taxonomy_tags')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('taxonomy_tags');
        $this->load->model( array('Taxonomy_tags_model') );
        
        $this->template_data->set('main_page', 'taxonomy' ); 
        $this->template_data->set('sub_page', 'taxonomy_tags' ); 
        $this->template_data->set('page_title', 'Tags' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Taxonomy_tags_model->setFilter( 'taxonomy_tags.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Taxonomy_tags_model->setLimit(100);
        
        $this->template_data->set('taxonomy_tags', $this->Taxonomy_tags_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('taxonomy_tags', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Taxonomy_tags_model->setStart($start);
	    $this->Taxonomy_tags_model->setLimit(100);


 
        $this->template_data->set('taxonomy_tags', $this->Taxonomy_tags_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Tags" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_taxonomy_tags')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Tag" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($tag_id)
	{
		if( ! $this->session->userdata('controller_taxonomy_tags')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Taxonomy_tags_model->setTagId($tag_id);
	    $current_item = $this->Taxonomy_tags_model->getByTagId();
        $this->template_data->set('taxonomy_tags',  $current_item);
        
        
        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->name ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($tag_id)
	{
		if( ! $this->session->userdata('controller_taxonomy_tags')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Taxonomy_tags_model->setTagId($tag_id);
         $this->template_data->set('taxonomy_tags', $this->Taxonomy_tags_model->deleteByTagId() );
         $this->output->set_header("location: " . site_url('taxonomy_tags') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('tag_name', 'lang:taxonomy_tags_tag_name', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('tag_id', 'lang:taxonomy_tags_tag_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Taxonomy_tags_model->setTagId( $this->input->post('tag_id') );
			$this->Taxonomy_tags_model->setTagName( $this->input->post('tag_name') );

			if( $action == 'edit' ) {
				$this->Taxonomy_tags_model->setTagSlug( url_title($this->input->post('tag_slug'), '-', TRUE) );
			} else {
				$this->Taxonomy_tags_model->setTagSlug(  url_title($this->input->post('tag_name'), '-', TRUE) );
			}

			$this->Taxonomy_tags_model->setTagActive( $this->input->post('tag_active') );

			if( $action == 'add' ) {
				if( $this->Taxonomy_tags_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Taxonomy_tags_model->updateByTagId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file taxonomy_tags.php */
/* Location: ./application/controllers/taxonomy_tags.php */