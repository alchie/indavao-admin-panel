<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_videos')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('videos');
        $this->load->model( array('Videos_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'videos' ); 
        $this->template_data->set('page_title', 'Videos' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Videos_model->setFilter( 'videos.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Videos_model->setLimit(100);
        
		$this->Videos_model->setJoin('videos as videos_Ahn', 'videos.video_parent = videos_Ahn.video_id');
		$this->Videos_model->setSelect('videos.*');
		$this->Videos_model->setSelect('videos_Ahn.video_title as parent_title');


        $this->template_data->set('videos', $this->Videos_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('videos', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Videos_model->setStart($start);
	    $this->Videos_model->setLimit(100);


		$this->Videos_model->setJoin('videos as videos_Ahn', 'videos.video_parent = videos_Ahn.video_id');
		$this->Videos_model->setSelect('videos.*');
		$this->Videos_model->setSelect('videos_Ahn.video_title as parent_title');


 
        $this->template_data->set('videos', $this->Videos_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Videos" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_videos')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Videos_model->setOrder('video_title', 'ASC');
		$this->Videos_model->setLimit(0);
		$this->Videos_model->limitDataFields(array('video_id','video_title'));
		$this->template_data->set('videos_video_parent', $this->Videos_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Video" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($video_id)
	{
		if( ! $this->session->userdata('controller_videos')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Videos_model->setVideoId($video_id);
	    $current_item = $this->Videos_model->getByVideoId();
        $this->template_data->set('videos',  $current_item);
        
        
		$this->Videos_model->setOrder('video_title', 'ASC');
		$this->Videos_model->setLimit(0);
		$this->Videos_model->limitDataFields(array('video_id','video_title'));
		$this->template_data->set('videos_video_parent', $this->Videos_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->video_title ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($video_id)
	{
		if( ! $this->session->userdata('controller_videos')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Videos_model->setVideoId($video_id);
         $this->template_data->set('videos', $this->Videos_model->deleteByVideoId() );
         $this->output->set_header("location: " . site_url('videos') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('video_title', 'lang:videos_video_title', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('video_id', 'lang:videos_video_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Videos_model->setVideoId( $this->input->post('video_id') );
			$this->Videos_model->setVideoUrl( $this->input->post('video_url') );
			$this->Videos_model->setVideoTitle( $this->input->post('video_title') );
			$this->Videos_model->setVideoDescription( $this->input->post('video_description') );
			$this->Videos_model->setVideoImage( $this->input->post('video_image') );
			$this->Videos_model->setVideoSource( $this->input->post('video_source') );
			$this->Videos_model->setVideoParent( $this->input->post('video_parent') );
			$this->Videos_model->setVideoAdded( $this->input->post('video_added') );
			$this->Videos_model->setVideoPriority( $this->input->post('video_priority') );

			if( $action == 'edit' ) {
				$this->Videos_model->setVideoSlug( url_title($this->input->post('video_slug'), '-', TRUE) );
			} else {
				$this->Videos_model->setVideoSlug(  url_title($this->input->post('video_title'), '-', TRUE) );
			}

			$this->Videos_model->setVideoActive( $this->input->post('video_active') );

			if( $action == 'add' ) {
				if( $this->Videos_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Videos_model->updateByVideoId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file videos.php */
/* Location: ./application/controllers/videos.php */