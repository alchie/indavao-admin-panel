<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        if( ! $this->session->userdata('admin_logged_in') )
        {
           redirect('login', 'location', 301);
		} else {
		    redirect('dashboard', 'location', 301);
		}
        
    }
    
    
}


