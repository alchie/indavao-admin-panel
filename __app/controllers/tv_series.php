<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tv_series extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_tv_series')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('tv_series');
        $this->load->model( array('Tv_series_model', 'Taxonomy_tags_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'tv_series' ); 
        $this->template_data->set('page_title', 'Series' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Tv_series_model->setFilter( 'tv_series.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Tv_series_model->setLimit(100);
        
		$this->Tv_series_model->setJoin('taxonomy_tags as taxonomy_tags_NQg', 'tv_series.series_tag = taxonomy_tags_NQg.tag_id');
		$this->Tv_series_model->setSelect('tv_series.*');
		$this->Tv_series_model->setSelect('taxonomy_tags_NQg.tag_name as tag_name');


        $this->template_data->set('tv_series', $this->Tv_series_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('tv_series', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Tv_series_model->setStart($start);
	    $this->Tv_series_model->setLimit(100);


		$this->Tv_series_model->setJoin('taxonomy_tags as taxonomy_tags_NQg', 'tv_series.series_tag = taxonomy_tags_NQg.tag_id');
		$this->Tv_series_model->setSelect('tv_series.*');
		$this->Tv_series_model->setSelect('taxonomy_tags_NQg.tag_name as tag_name');


 
        $this->template_data->set('tv_series', $this->Tv_series_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Series" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_tv_series')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_series_tag', $this->Taxonomy_tags_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Series" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($series_id)
	{
		if( ! $this->session->userdata('controller_tv_series')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Tv_series_model->setSeriesId($series_id);
	    $current_item = $this->Tv_series_model->getBySeriesId();
        $this->template_data->set('tv_series',  $current_item);
        
        
		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_series_tag', $this->Taxonomy_tags_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->series_title ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($series_id)
	{
		if( ! $this->session->userdata('controller_tv_series')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Tv_series_model->setSeriesId($series_id);
         $this->template_data->set('tv_series', $this->Tv_series_model->deleteBySeriesId() );
         $this->output->set_header("location: " . site_url('tv_series') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('series_title', 'lang:tv_series_series_title', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('series_id', 'lang:tv_series_series_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Tv_series_model->setSeriesId( $this->input->post('series_id') );
			$this->Tv_series_model->setSeriesTitle( $this->input->post('series_title') );

			if( $action == 'edit' ) {
				$this->Tv_series_model->setSeriesSlug( url_title($this->input->post('series_slug'), '-', TRUE) );
			} else {
				$this->Tv_series_model->setSeriesSlug(  url_title($this->input->post('series_title'), '-', TRUE) );
			}

			$this->Tv_series_model->setSeriesDescription( $this->input->post('series_description') );
			$this->Tv_series_model->setSeriesYear( $this->input->post('series_year') );
			$this->Tv_series_model->setSeriesLang( $this->input->post('series_lang') );
			$this->Tv_series_model->setSeriesPoster( $this->input->post('series_poster') );
			$this->Tv_series_model->setSeriesTag( $this->input->post('series_tag') );
			$this->Tv_series_model->setSeriesActive( $this->input->post('series_active') );

			if( $action == 'add' ) {
				if( $this->Tv_series_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Tv_series_model->updateBySeriesId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file tv_series.php */
/* Location: ./application/controllers/tv_series.php */