<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins_access extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_admins_access')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('admin_id') == '') {
                redirect('admins','location');
        } 
        $this->template_data->set('filter_key', 'admin_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('admins_access');
        $this->load->model( array('Admins_access_model', 'Admins_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'admins' ); 
        $this->template_data->set('page_title', 'Controller Access' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Admins_access_model->setFilter( 'admins_access.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('admin_id') != '') {
                $this->Admins_access_model->setFilter( 'admins_access.admin_id', $this->input->get('admin_id') );
        }
        
                $this->Admins_model->setId($this->input->get('admin_id'), true);
        $parentData = $this->Admins_model->get();
        $this->template_data->page_title( $parentData->username );
        
        $this->Admins_access_model->setLimit(100);
        
		$this->Admins_access_model->setJoin('admins as admins_oWR', 'admins_access.admin_id = admins_oWR.id');
		$this->Admins_access_model->setSelect('admins_access.*');
		$this->Admins_access_model->setSelect('admins_oWR.username as username');


        $this->template_data->set('admins_access', $this->Admins_access_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('admins_access', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Admins_access_model->setStart($start);
	    $this->Admins_access_model->setLimit(100);


		$this->Admins_access_model->setJoin('admins as admins_oWR', 'admins_access.admin_id = admins_oWR.id');
		$this->Admins_access_model->setSelect('admins_access.*');
		$this->Admins_access_model->setSelect('admins_oWR.username as username');


 
        $this->template_data->set('admins_access', $this->Admins_access_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Controller Access" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins_access', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_admins_access')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Admins_model->setOrder('username', 'ASC');
		$this->Admins_model->setLimit(0);
		$this->Admins_model->limitDataFields(array('id','username'));
		$this->template_data->set('admins_admin_id', $this->Admins_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Controller Access" ); 
	    
        $this->Admins_model->setId($this->input->get('admin_id'), true);
        $parentData = $this->Admins_model->get();
        $this->template_data->page_title( $parentData->username );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins_access', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_admins_access')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Admins_access_model->setId($id);
	    $current_item = $this->Admins_access_model->getById();
        $this->template_data->set('admins_access',  $current_item);
        
        
		$this->Admins_model->setOrder('username', 'ASC');
		$this->Admins_model->setLimit(0);
		$this->Admins_model->limitDataFields(array('id','username'));
		$this->template_data->set('admins_admin_id', $this->Admins_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Admins_model->setId($this->input->get('admin_id'), true);
        $parentData = $this->Admins_model->get();
        $this->template_data->page_title( $parentData->username );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins_access', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_admins_access')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Admins_access_model->setId($id);
         $this->template_data->set('admins_access', $this->Admins_access_model->deleteById() );
         $this->output->set_header("location: " . site_url('admins_access') . "?admin_id=" . $this->input->get('admin_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('controller', 'lang:admins_access_controller', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:admins_access_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Admins_access_model->setId( $this->input->post('id') );
			$this->Admins_access_model->setAdminId( $this->input->post('admin_id') );
			$this->Admins_access_model->setController( $this->input->post('controller') );
			$this->Admins_access_model->setAdd( $this->input->post('add') );
			$this->Admins_access_model->setEdit( $this->input->post('edit') );
			$this->Admins_access_model->setDelete( $this->input->post('delete') );

			if( $action == 'add' ) {
				if( $this->Admins_access_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Admins_access_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file admins_access.php */
/* Location: ./application/controllers/admins_access.php */