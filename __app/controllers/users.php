<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_users')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('users');
        $this->load->model( array('Users_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'users' ); 
        $this->template_data->set('page_title', 'Users' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Users_model->setFilter( 'users.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Users_model->setLimit(100);
        
		$this->Users_model->setJoin('users as users_skB', 'users.referrer = users_skB.id');
		$this->Users_model->setSelect('users.*');
		$this->Users_model->setSelect('users_skB.fullname as user_fullname');


        $this->template_data->set('users', $this->Users_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('users', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Users_model->setStart($start);
	    $this->Users_model->setLimit(100);


		$this->Users_model->setJoin('users as users_skB', 'users.referrer = users_skB.id');
		$this->Users_model->setSelect('users.*');
		$this->Users_model->setSelect('users_skB.fullname as user_fullname');


 
        $this->template_data->set('users', $this->Users_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Users" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('users', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_users')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Users_model->setOrder('fullname', 'ASC');
		$this->Users_model->setLimit(0);
		$this->Users_model->limitDataFields(array('id','fullname'));
		$this->template_data->set('users_referrer', $this->Users_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New User" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('users', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_users')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Users_model->setId($id);
	    $current_item = $this->Users_model->getById();
        $this->template_data->set('users',  $current_item);
        
        
		$this->Users_model->setOrder('fullname', 'ASC');
		$this->Users_model->setLimit(0);
		$this->Users_model->limitDataFields(array('id','fullname'));
		$this->template_data->set('users_referrer', $this->Users_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->fullname ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('users', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_users')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Users_model->setId($id);
         $this->template_data->set('users', $this->Users_model->deleteById() );
         $this->output->set_header("location: " . site_url('users') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('fullname', 'lang:users_fullname', 'required');
			$this->form_validation->set_rules('email', 'lang:users_email', 'required|is_unique[users.email]|valid_email');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|valid_email');
			$this->form_validation->set_rules('user_id', 'lang:users_user_id', 'required|is_unique[users.user_id]');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:users_id', 'required');
			$this->form_validation->set_rules('fullname', 'lang:users_fullname', 'required');
			$this->form_validation->set_rules('email', 'lang:users_email', 'required|valid_email');
			$this->form_validation->set_rules('user_id', 'lang:users_user_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Users_model->setId( $this->input->post('id') );
			$this->Users_model->setFullname( $this->input->post('fullname') );
			$this->Users_model->setEmail( $this->input->post('email') );
			if( ( $this->input->post('password') != '' ) && ($this->input->post('password') == $this->input->post('confirm_password')) ) {
				$this->Users_model->setPassword( $this->input->post('password') );
			} else {
				$this->Users_model->setExclude('password');
			}
			$this->Users_model->setUserId( $this->input->post('user_id') );
			$this->Users_model->setUsername( $this->input->post('username') );
			$this->Users_model->setLevel( $this->input->post('level') );
			$this->Users_model->setReferrer( $this->input->post('referrer') );
			$this->Users_model->setActive( $this->input->post('active') );
			$this->Users_model->setActivationCode( $this->input->post('activation_code') );
			$this->Users_model->setType( $this->input->post('type') );
			$this->Users_model->setDateJoined( $this->input->post('date_joined') );
			$this->Users_model->setIpJoined( $this->input->post('ip_joined') );
			$this->Users_model->setLastLogin( $this->input->post('last_login') );
			$this->Users_model->setLastLoginIp( $this->input->post('last_login_ip') );

			if( $action == 'add' ) {
				if( $this->Users_model->insert() ) {
					redirect('users/edit/' . $this->Users_model->getId());
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Users_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file users.php */
/* Location: ./application/controllers/users.php */