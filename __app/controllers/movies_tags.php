<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_tags extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_movies_tags')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('movie_id') == '') {
                redirect('movies','location');
        } 
        $this->template_data->set('filter_key', 'movie_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('movies_tags');
        $this->load->model( array('Movies_tags_model', 'Movies_model', 'Taxonomy_tags_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'movies' ); 
        $this->template_data->set('page_title', 'Tags' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Movies_tags_model->setFilter( 'movies_tags.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('movie_id') != '') {
                $this->Movies_tags_model->setFilter( 'movies_tags.movie_id', $this->input->get('movie_id') );
        }
        
                $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
        
        $this->Movies_tags_model->setLimit(100);
        
		$this->Movies_tags_model->setJoin('movies as movies_Grc', 'movies_tags.movie_id = movies_Grc.movie_id');
		$this->Movies_tags_model->setSelect('movies_tags.*');
		$this->Movies_tags_model->setSelect('movies_Grc.movie_title as movie_title');


		$this->Movies_tags_model->setJoin('taxonomy_tags as taxonomy_tags_ssB', 'movies_tags.tag_id = taxonomy_tags_ssB.tag_id');
		$this->Movies_tags_model->setSelect('movies_tags.*');
		$this->Movies_tags_model->setSelect('taxonomy_tags_ssB.tag_name as tag_name');


        $this->template_data->set('movies_tags', $this->Movies_tags_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('movies_tags', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Movies_tags_model->setStart($start);
	    $this->Movies_tags_model->setLimit(100);


		$this->Movies_tags_model->setJoin('movies as movies_Grc', 'movies_tags.movie_id = movies_Grc.movie_id');
		$this->Movies_tags_model->setSelect('movies_tags.*');
		$this->Movies_tags_model->setSelect('movies_Grc.movie_title as movie_title');


		$this->Movies_tags_model->setJoin('taxonomy_tags as taxonomy_tags_ssB', 'movies_tags.tag_id = taxonomy_tags_ssB.tag_id');
		$this->Movies_tags_model->setSelect('movies_tags.*');
		$this->Movies_tags_model->setSelect('taxonomy_tags_ssB.tag_name as tag_name');


 
        $this->template_data->set('movies_tags', $this->Movies_tags_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Tags" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_movies_tags')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Movies_model->setOrder('movie_title', 'ASC');
		$this->Movies_model->setLimit(0);
		$this->Movies_model->limitDataFields(array('movie_id','movie_title'));
		$this->template_data->set('movies_movie_id', $this->Movies_model->populate() );

		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_tag_id', $this->Taxonomy_tags_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Tag" ); 
	    
        $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_movies_tags')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Movies_tags_model->setId($id);
	    $current_item = $this->Movies_tags_model->getById();
        $this->template_data->set('movies_tags',  $current_item);
        
        
		$this->Movies_model->setOrder('movie_title', 'ASC');
		$this->Movies_model->setLimit(0);
		$this->Movies_model->limitDataFields(array('movie_id','movie_title'));
		$this->template_data->set('movies_movie_id', $this->Movies_model->populate() );

		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_tag_id', $this->Taxonomy_tags_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_tags', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_movies_tags')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Movies_tags_model->setId($id);
         $this->template_data->set('movies_tags', $this->Movies_tags_model->deleteById() );
         $this->output->set_header("location: " . site_url('movies_tags') . "?movie_id=" . $this->input->get('movie_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:movies_tags_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Movies_tags_model->setId( $this->input->post('id') );
			$this->Movies_tags_model->setMovieId( $this->input->post('movie_id') );
			$this->Movies_tags_model->setTagId( $this->input->post('tag_id') );

			if( $action == 'add' ) {
				if( $this->Movies_tags_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Movies_tags_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file movies_tags.php */
/* Location: ./application/controllers/movies_tags.php */