<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_projects_pictures extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_realestate_projects_pictures')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('project_id') == '') {
                redirect('realestate_projects','location');
        } 
        $this->template_data->set('filter_key', 'project_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('realestate_projects_pictures');
        $this->load->model( array('Realestate_projects_pictures_model', 'Realestate_projects_model') );
        
        $this->template_data->set('main_page', 'ads' ); 
        $this->template_data->set('sub_page', 'realestate_projects' ); 
        $this->template_data->set('page_title', 'Pictures' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Realestate_projects_pictures_model->setFilter( 'realestate_projects_pictures.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('project_id') != '') {
                $this->Realestate_projects_pictures_model->setFilter( 'realestate_projects_pictures.project_id', $this->input->get('project_id') );
        }
        
                $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
        
        $this->Realestate_projects_pictures_model->setLimit(100);
        
		$this->Realestate_projects_pictures_model->setJoin('realestate_projects as realestate_projects_icQ', 'realestate_projects_pictures.project_id = realestate_projects_icQ.id');
		$this->Realestate_projects_pictures_model->setSelect('realestate_projects_pictures.*');
		$this->Realestate_projects_pictures_model->setSelect('realestate_projects_icQ.name as project_name');


        $this->template_data->set('realestate_projects_pictures', $this->Realestate_projects_pictures_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('realestate_projects_pictures', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Realestate_projects_pictures_model->setStart($start);
	    $this->Realestate_projects_pictures_model->setLimit(100);


		$this->Realestate_projects_pictures_model->setJoin('realestate_projects as realestate_projects_icQ', 'realestate_projects_pictures.project_id = realestate_projects_icQ.id');
		$this->Realestate_projects_pictures_model->setSelect('realestate_projects_pictures.*');
		$this->Realestate_projects_pictures_model->setSelect('realestate_projects_icQ.name as project_name');


 
        $this->template_data->set('realestate_projects_pictures', $this->Realestate_projects_pictures_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Pictures" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects_pictures', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_realestate_projects_pictures')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Realestate_projects_model->setOrder('name', 'ASC');
		$this->Realestate_projects_model->setLimit(0);
		$this->Realestate_projects_model->limitDataFields(array('id','name'));
		$this->template_data->set('realestate_projects_project_id', $this->Realestate_projects_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Picture" ); 
	    
        $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects_pictures', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_realestate_projects_pictures')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Realestate_projects_pictures_model->setId($id);
	    $current_item = $this->Realestate_projects_pictures_model->getById();
        $this->template_data->set('realestate_projects_pictures',  $current_item);
        
        
		$this->Realestate_projects_model->setOrder('name', 'ASC');
		$this->Realestate_projects_model->setLimit(0);
		$this->Realestate_projects_model->limitDataFields(array('id','name'));
		$this->template_data->set('realestate_projects_project_id', $this->Realestate_projects_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Realestate_projects_model->setId($this->input->get('project_id'), true);
        $parentData = $this->Realestate_projects_model->get();
        $this->template_data->page_title( $parentData->name );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects_pictures', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_realestate_projects_pictures')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Realestate_projects_pictures_model->setId($id);
         $this->template_data->set('realestate_projects_pictures', $this->Realestate_projects_pictures_model->deleteById() );
         $this->output->set_header("location: " . site_url('realestate_projects_pictures') . "?project_id=" . $this->input->get('project_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('project_id', 'lang:realestate_projects_pictures_project_id', 'required');
			$this->form_validation->set_rules('url', 'lang:realestate_projects_pictures_url', 'required|prep_url');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:realestate_projects_pictures_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Realestate_projects_pictures_model->setId( $this->input->post('id') );
			$this->Realestate_projects_pictures_model->setProjectId( $this->input->post('project_id') );
			$this->Realestate_projects_pictures_model->setUrl( $this->input->post('url') );

			if( $action == 'add' ) {
				if( $this->Realestate_projects_pictures_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Realestate_projects_pictures_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file realestate_projects_pictures.php */
/* Location: ./application/controllers/realestate_projects_pictures.php */