<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taxonomy_genre extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_taxonomy_genre')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('taxonomy_genre');
        $this->load->model( array('Taxonomy_genre_model') );
        
        $this->template_data->set('main_page', 'taxonomy' ); 
        $this->template_data->set('sub_page', 'taxonomy_genre' ); 
        $this->template_data->set('page_title', 'Genre' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Taxonomy_genre_model->setFilter( 'taxonomy_genre.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Taxonomy_genre_model->setLimit(100);
        
        $this->template_data->set('taxonomy_genre', $this->Taxonomy_genre_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('taxonomy_genre', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Taxonomy_genre_model->setStart($start);
	    $this->Taxonomy_genre_model->setLimit(100);


 
        $this->template_data->set('taxonomy_genre', $this->Taxonomy_genre_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Genre" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_taxonomy_genre')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Genre" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($genre_id)
	{
		if( ! $this->session->userdata('controller_taxonomy_genre')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Taxonomy_genre_model->setGenreId($genre_id);
	    $current_item = $this->Taxonomy_genre_model->getByGenreId();
        $this->template_data->set('taxonomy_genre',  $current_item);
        
        
        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->genre_name ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($genre_id)
	{
		if( ! $this->session->userdata('controller_taxonomy_genre')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Taxonomy_genre_model->setGenreId($genre_id);
         $this->template_data->set('taxonomy_genre', $this->Taxonomy_genre_model->deleteByGenreId() );
         $this->output->set_header("location: " . site_url('taxonomy_genre') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('genre_name', 'lang:taxonomy_genre_genre_name', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('genre_id', 'lang:taxonomy_genre_genre_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Taxonomy_genre_model->setGenreId( $this->input->post('genre_id') );
			$this->Taxonomy_genre_model->setGenreName( $this->input->post('genre_name') );

			if( $action == 'edit' ) {
				$this->Taxonomy_genre_model->setGenreSlug( url_title($this->input->post('genre_slug'), '-', TRUE) );
			} else {
				$this->Taxonomy_genre_model->setGenreSlug(  url_title($this->input->post('genre_name'), '-', TRUE) );
			}

			$this->Taxonomy_genre_model->setGenreActive( $this->input->post('genre_active') );

			if( $action == 'add' ) {
				if( $this->Taxonomy_genre_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Taxonomy_genre_model->updateByGenreId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file taxonomy_genre.php */
/* Location: ./application/controllers/taxonomy_genre.php */