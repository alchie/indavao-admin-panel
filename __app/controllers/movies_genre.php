<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_genre extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_movies_genre')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('movie_id') == '') {
                redirect('movies','location');
        } 
        $this->template_data->set('filter_key', 'movie_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('movies_genre');
        $this->load->model( array('Movies_genre_model', 'Movies_model', 'Taxonomy_genre_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'movies' ); 
        $this->template_data->set('page_title', 'Genre' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Movies_genre_model->setFilter( 'movies_genre.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('movie_id') != '') {
                $this->Movies_genre_model->setFilter( 'movies_genre.movie_id', $this->input->get('movie_id') );
        }
        
                $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
        
        $this->Movies_genre_model->setLimit(100);
        
		$this->Movies_genre_model->setJoin('movies as movies_fHP', 'movies_genre.movie_id = movies_fHP.movie_id');
		$this->Movies_genre_model->setSelect('movies_genre.*');
		$this->Movies_genre_model->setSelect('movies_fHP.movie_title as movie_title');


		$this->Movies_genre_model->setJoin('taxonomy_genre as taxonomy_genre_nUG', 'movies_genre.genre_id = taxonomy_genre_nUG.genre_id');
		$this->Movies_genre_model->setSelect('movies_genre.*');
		$this->Movies_genre_model->setSelect('taxonomy_genre_nUG.genre_name as genre_name');


        $this->template_data->set('movies_genre', $this->Movies_genre_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('movies_genre', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Movies_genre_model->setStart($start);
	    $this->Movies_genre_model->setLimit(100);


		$this->Movies_genre_model->setJoin('movies as movies_fHP', 'movies_genre.movie_id = movies_fHP.movie_id');
		$this->Movies_genre_model->setSelect('movies_genre.*');
		$this->Movies_genre_model->setSelect('movies_fHP.movie_title as movie_title');


		$this->Movies_genre_model->setJoin('taxonomy_genre as taxonomy_genre_nUG', 'movies_genre.genre_id = taxonomy_genre_nUG.genre_id');
		$this->Movies_genre_model->setSelect('movies_genre.*');
		$this->Movies_genre_model->setSelect('taxonomy_genre_nUG.genre_name as genre_name');


 
        $this->template_data->set('movies_genre', $this->Movies_genre_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Genre" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_movies_genre')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Movies_model->setOrder('movie_title', 'ASC');
		$this->Movies_model->setLimit(0);
		$this->Movies_model->limitDataFields(array('movie_id','movie_title'));
		$this->template_data->set('movies_movie_id', $this->Movies_model->populate() );

		$this->Taxonomy_genre_model->setOrder('genre_name', 'ASC');
		$this->Taxonomy_genre_model->setLimit(0);
		$this->Taxonomy_genre_model->limitDataFields(array('genre_id','genre_name'));
		$this->template_data->set('taxonomy_genre_genre_id', $this->Taxonomy_genre_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Genre" ); 
	    
        $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_movies_genre')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Movies_genre_model->setId($id);
	    $current_item = $this->Movies_genre_model->getById();
        $this->template_data->set('movies_genre',  $current_item);
        
        
		$this->Movies_model->setOrder('movie_title', 'ASC');
		$this->Movies_model->setLimit(0);
		$this->Movies_model->limitDataFields(array('movie_id','movie_title'));
		$this->template_data->set('movies_movie_id', $this->Movies_model->populate() );

		$this->Taxonomy_genre_model->setOrder('genre_name', 'ASC');
		$this->Taxonomy_genre_model->setLimit(0);
		$this->Taxonomy_genre_model->limitDataFields(array('genre_id','genre_name'));
		$this->template_data->set('taxonomy_genre_genre_id', $this->Taxonomy_genre_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Movies_model->setMovieId($this->input->get('movie_id'), true);
        $parentData = $this->Movies_model->get();
        $this->template_data->page_title( $parentData->movie_title );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_movies_genre')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Movies_genre_model->setId($id);
         $this->template_data->set('movies_genre', $this->Movies_genre_model->deleteById() );
         $this->output->set_header("location: " . site_url('movies_genre') . "?movie_id=" . $this->input->get('movie_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('movie_id', 'lang:movies_genre_movie_id', 'required');
			$this->form_validation->set_rules('genre_id', 'lang:movies_genre_genre_id', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:movies_genre_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Movies_genre_model->setId( $this->input->post('id') );
			$this->Movies_genre_model->setMovieId( $this->input->post('movie_id') );
			$this->Movies_genre_model->setGenreId( $this->input->post('genre_id') );

			if( $action == 'add' ) {
				if( $this->Movies_genre_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Movies_genre_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file movies_genre.php */
/* Location: ./application/controllers/movies_genre.php */