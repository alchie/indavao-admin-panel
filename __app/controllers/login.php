<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('template_data');
    }
    	
	public function index()
	{
	    
	    $this->load->library(array('form_validation') );
	    
	    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|md5|sha1');
		
		if ($this->form_validation->run() == FALSE)
		{
		     if( $this->input->post() ) {
		         $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else 
		{
		    
		    $this->load->model(array('Admins_model', 'Admins_access_model'));
		    $this->Admins_model->setUsername( $this->input->post( 'username'), true );
            $this->Admins_model->setPassword( $this->input->post( 'password'), true );
		    $this->Admins_model->setActive( 1, true );
		    
		    if ( $this->Admins_model->nonempty() === TRUE ) { 
				  $admin = $this->Admins_model->getResults();
				  
				  $userdata = array(
						'admin_logged_in' => TRUE,
						'admin_username' => $admin->username,
						'admin_id' => $admin->id,
						'admin_email' => $admin->email,
						'last_login' => $admin->last_login,
						'last_login_ip' => $admin->last_login_ip
					);
				  
				  $this->Admins_access_model->setLimit(0);
				  $this->Admins_access_model->setAdminId( $admin->id, true );
				  $admin_access = $this->Admins_access_model->populate();
				  if( $admin_access ) foreach( $admin_access as $access ) {
					  $userdata['controller_' . $access->controller] = (object) array( 
					  'can_add' => (bool) $access->add,
					  'can_edit' => (bool) $access->edit,
					  'can_delete' => (bool) $access->delete
					  );
				  }
				  $this->session->set_userdata( $userdata );
				  
					$this->load->helper( array('url', 'date') );
					$this->Admins_model->setLastLogin( unix_to_human( time(), TRUE, 'eu' ), false, true );
					$this->Admins_model->setLastLoginIp( $this->input->ip_address(), false, true );
					$this->Admins_model->update();		       
					
				   redirect('dashboard', 'location', 301);
		    } else {
				$this->template_data->alert( 'Unable to login!', 'danger');
			}
		}
		
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('login', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );		

	}
	
	public function logout() {
	    $this->session->sess_destroy();
	    redirect('login', 'refresh');
	    exit;
	}
	
}

