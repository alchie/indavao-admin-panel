<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Multisites extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_multisites')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('multisites');
        $this->load->model( array('Multisites_model') );
        
        $this->template_data->set('main_page', 'settings' ); 
        $this->template_data->set('sub_page', 'multisites' ); 
        $this->template_data->set('page_title', 'Websites' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Multisites_model->setFilter( 'multisites.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Multisites_model->setLimit(100);
        
        $this->template_data->set('multisites', $this->Multisites_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('multisites', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Multisites_model->setStart($start);
	    $this->Multisites_model->setLimit(100);


 
        $this->template_data->set('multisites', $this->Multisites_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Websites" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('multisites', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_multisites')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Website" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('multisites', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($site_id)
	{
		if( ! $this->session->userdata('controller_multisites')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Multisites_model->setSiteId($site_id);
	    $current_item = $this->Multisites_model->getBySiteId();
        $this->template_data->set('multisites',  $current_item);
        
        
        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->title ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('multisites', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($site_id)
	{
		if( ! $this->session->userdata('controller_multisites')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Multisites_model->setSiteId($site_id);
         $this->template_data->set('multisites', $this->Multisites_model->deleteBySiteId() );
         $this->output->set_header("location: " . site_url('multisites') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('site_name', 'lang:multisites_site_name', 'required');
			$this->form_validation->set_rules('title', 'lang:multisites_title', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('site_id', 'lang:multisites_site_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Multisites_model->setSiteId( $this->input->post('site_id') );
			$this->Multisites_model->setSiteName( $this->input->post('site_name') );
			$this->Multisites_model->setTitle( $this->input->post('title') );
			$this->Multisites_model->setDescription( $this->input->post('description') );
			$this->Multisites_model->setUrl( $this->input->post('url') );

			if( $action == 'add' ) {
				if( $this->Multisites_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Multisites_model->updateBySiteId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file multisites.php */
/* Location: ./application/controllers/multisites.php */