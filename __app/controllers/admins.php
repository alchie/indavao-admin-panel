<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_admins')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('admins');
        $this->load->model( array('Admins_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'admins' ); 
        $this->template_data->set('page_title', 'Admins' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Admins_model->setFilter( 'admins.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Admins_model->setLimit(100);
        
        $this->template_data->set('admins', $this->Admins_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('admins', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Admins_model->setStart($start);
	    $this->Admins_model->setLimit(100);


 
        $this->template_data->set('admins', $this->Admins_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Admins" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_admins')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Admins" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_admins')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Admins_model->setId($id);
	    $current_item = $this->Admins_model->getById();
        $this->template_data->set('admins',  $current_item);
        
        
        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->username ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('admins', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_admins')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Admins_model->setId($id);
         $this->template_data->set('admins', $this->Admins_model->deleteById() );
         $this->output->set_header("location: " . site_url('admins') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('email', 'lang:admins_email', 'required|is_unique[admins.email]|valid_email');
			$this->form_validation->set_rules('username', 'lang:admins_username', 'required|is_unique[admins.username]');
			$this->form_validation->set_rules('password', 'lang:admins_password', 'trim|xss_clean|md5|sha1');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|xss_clean|md5|sha1');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:admins_id', 'required');
			$this->form_validation->set_rules('email', 'lang:admins_email', 'required|valid_email');
			$this->form_validation->set_rules('username', 'lang:admins_username', 'required');
			$this->form_validation->set_rules('password', 'lang:admins_password', 'trim|xss_clean|md5|sha1');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|xss_clean|md5|sha1');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Admins_model->setId( $this->input->post('id') );
			$this->Admins_model->setEmail( $this->input->post('email') );
			$this->Admins_model->setUsername( $this->input->post('username') );
			if( ( $this->input->post('password') != '' ) && ($this->input->post('password') == $this->input->post('confirm_password')) ) {
				$this->Admins_model->setPassword( $this->input->post('password') );
			} else {
				$this->Admins_model->setExclude('password');
			}
			$this->Admins_model->setActive( $this->input->post('active') );
			$this->Admins_model->setDateCreated( $this->input->post('date_created') );
			$this->Admins_model->setLastLogin( $this->input->post('last_login') );
			$this->Admins_model->setLastLoginIp( $this->input->post('last_login_ip') );

			if( $action == 'add' ) {
				if( $this->Admins_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Admins_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file admins.php */
/* Location: ./application/controllers/admins.php */