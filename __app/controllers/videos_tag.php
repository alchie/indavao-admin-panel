<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_tag extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_videos_tag')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('video_id') == '') {
                redirect('videos','location');
        } 
        $this->template_data->set('filter_key', 'video_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('videos_tag');
        $this->load->model( array('Videos_tag_model', 'Videos_model', 'Taxonomy_tags_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'videos' ); 
        $this->template_data->set('page_title', 'Tags' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Videos_tag_model->setFilter( 'videos_tag.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('video_id') != '') {
                $this->Videos_tag_model->setFilter( 'videos_tag.video_id', $this->input->get('video_id') );
        }
        
                $this->Videos_model->setVideoId($this->input->get('video_id'), true);
        $parentData = $this->Videos_model->get();
        $this->template_data->page_title( $parentData->video_title );
        
        $this->Videos_tag_model->setLimit(100);
        
		$this->Videos_tag_model->setJoin('videos as videos_oLO', 'videos_tag.video_id = videos_oLO.video_id');
		$this->Videos_tag_model->setSelect('videos_tag.*');
		$this->Videos_tag_model->setSelect('videos_oLO.video_title as video_title');


		$this->Videos_tag_model->setJoin('taxonomy_tags as taxonomy_tags_OgM', 'videos_tag.tag_id = taxonomy_tags_OgM.tag_id');
		$this->Videos_tag_model->setSelect('videos_tag.*');
		$this->Videos_tag_model->setSelect('taxonomy_tags_OgM.tag_name as tag_name');


        $this->template_data->set('videos_tag', $this->Videos_tag_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('videos_tag', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Videos_tag_model->setStart($start);
	    $this->Videos_tag_model->setLimit(100);


		$this->Videos_tag_model->setJoin('videos as videos_oLO', 'videos_tag.video_id = videos_oLO.video_id');
		$this->Videos_tag_model->setSelect('videos_tag.*');
		$this->Videos_tag_model->setSelect('videos_oLO.video_title as video_title');


		$this->Videos_tag_model->setJoin('taxonomy_tags as taxonomy_tags_OgM', 'videos_tag.tag_id = taxonomy_tags_OgM.tag_id');
		$this->Videos_tag_model->setSelect('videos_tag.*');
		$this->Videos_tag_model->setSelect('taxonomy_tags_OgM.tag_name as tag_name');


 
        $this->template_data->set('videos_tag', $this->Videos_tag_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Tags" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos_tag', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_videos_tag')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Videos_model->setOrder('video_title', 'ASC');
		$this->Videos_model->setLimit(0);
		$this->Videos_model->limitDataFields(array('video_id','video_title'));
		$this->template_data->set('videos_video_id', $this->Videos_model->populate() );

		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_tag_id', $this->Taxonomy_tags_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Tag" ); 
	    
        $this->Videos_model->setVideoId($this->input->get('video_id'), true);
        $parentData = $this->Videos_model->get();
        $this->template_data->page_title( $parentData->video_title );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos_tag', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($vt_id)
	{
		if( ! $this->session->userdata('controller_videos_tag')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Videos_tag_model->setVtId($vt_id);
	    $current_item = $this->Videos_tag_model->getByVtId();
        $this->template_data->set('videos_tag',  $current_item);
        
        
		$this->Videos_model->setOrder('video_title', 'ASC');
		$this->Videos_model->setLimit(0);
		$this->Videos_model->limitDataFields(array('video_id','video_title'));
		$this->template_data->set('videos_video_id', $this->Videos_model->populate() );

		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_tag_id', $this->Taxonomy_tags_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Videos_model->setVideoId($this->input->get('video_id'), true);
        $parentData = $this->Videos_model->get();
        $this->template_data->page_title( $parentData->video_title );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('videos_tag', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($vt_id)
	{
		if( ! $this->session->userdata('controller_videos_tag')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Videos_tag_model->setVtId($vt_id);
         $this->template_data->set('videos_tag', $this->Videos_tag_model->deleteByVtId() );
         $this->output->set_header("location: " . site_url('videos_tag') . "?video_id=" . $this->input->get('video_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('vt_id', 'lang:videos_tag_vt_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Videos_tag_model->setVtId( $this->input->post('vt_id') );
			$this->Videos_tag_model->setVideoId( $this->input->post('video_id') );
			$this->Videos_tag_model->setTagId( $this->input->post('tag_id') );

			if( $action == 'add' ) {
				if( $this->Videos_tag_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Videos_tag_model->updateByVtId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file videos_tag.php */
/* Location: ./application/controllers/videos_tag.php */