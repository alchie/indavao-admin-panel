<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_movies')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('movies');
        $this->load->model( array('Movies_model', 'Taxonomy_tags_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'movies' ); 
        $this->template_data->set('page_title', 'Movies' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Movies_model->setFilter( 'movies.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Movies_model->setLimit(100);
        
		$this->Movies_model->setJoin('taxonomy_tags as taxonomy_tags_xKc', 'movies.movie_tag = taxonomy_tags_xKc.tag_id');
		$this->Movies_model->setSelect('movies.*');
		$this->Movies_model->setSelect('taxonomy_tags_xKc.tag_name as tag_name');


        $this->template_data->set('movies', $this->Movies_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('movies', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Movies_model->setStart($start);
	    $this->Movies_model->setLimit(100);


		$this->Movies_model->setJoin('taxonomy_tags as taxonomy_tags_xKc', 'movies.movie_tag = taxonomy_tags_xKc.tag_id');
		$this->Movies_model->setSelect('movies.*');
		$this->Movies_model->setSelect('taxonomy_tags_xKc.tag_name as tag_name');


 
        $this->template_data->set('movies', $this->Movies_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Movies" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_movies')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_movie_tag', $this->Taxonomy_tags_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Movies" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($movie_id)
	{
		if( ! $this->session->userdata('controller_movies')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Movies_model->setMovieId($movie_id);
	    $current_item = $this->Movies_model->getByMovieId();
        $this->template_data->set('movies',  $current_item);
        
        
		$this->Taxonomy_tags_model->setOrder('tag_name', 'ASC');
		$this->Taxonomy_tags_model->setLimit(0);
		$this->Taxonomy_tags_model->limitDataFields(array('tag_id','tag_name'));
		$this->template_data->set('taxonomy_tags_movie_tag', $this->Taxonomy_tags_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->movie_title ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('movies', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($movie_id)
	{
		if( ! $this->session->userdata('controller_movies')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Movies_model->setMovieId($movie_id);
         $this->template_data->set('movies', $this->Movies_model->deleteByMovieId() );
         $this->output->set_header("location: " . site_url('movies') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('movie_title', 'lang:movies_movie_title', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('movie_id', 'lang:movies_movie_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Movies_model->setMovieId( $this->input->post('movie_id') );
			$this->Movies_model->setMovieTitle( $this->input->post('movie_title') );
			$this->Movies_model->setMovieDescription( $this->input->post('movie_description') );
			$this->Movies_model->setMovieYear( $this->input->post('movie_year') );
			$this->Movies_model->setMovieLang( $this->input->post('movie_lang') );

			if( $action == 'edit' ) {
				$this->Movies_model->setMovieSlug( url_title($this->input->post('movie_slug'), '-', TRUE) );
			} else {
				$this->Movies_model->setMovieSlug(  url_title($this->input->post('movie_title'), '-', TRUE) );
			}

			$this->Movies_model->setMoviePoster( $this->input->post('movie_poster') );
			$this->Movies_model->setMovieImdb( $this->input->post('movie_imdb') );
			$this->Movies_model->setMovieTag( $this->input->post('movie_tag') );
			$this->Movies_model->setMovieActive( $this->input->post('movie_active') );

			if( $action == 'add' ) {
				if( $this->Movies_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Movies_model->updateByMovieId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file movies.php */
/* Location: ./application/controllers/movies.php */