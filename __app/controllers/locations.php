<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_locations')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('locations');
        $this->load->model( array('Locations_model') );
        
        $this->template_data->set('main_page', 'taxonomy' ); 
        $this->template_data->set('sub_page', 'locations' ); 
        $this->template_data->set('page_title', 'Locations' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Locations_model->setFilter( 'locations.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Locations_model->setLimit(100);
        
		$this->Locations_model->setJoin('locations as locations_cAL', 'locations.parent_id = locations_cAL.id');
		$this->Locations_model->setSelect('locations.*');
		$this->Locations_model->setSelect('locations_cAL.name as parent_name');


        $this->template_data->set('locations', $this->Locations_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('locations', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Locations_model->setStart($start);
	    $this->Locations_model->setLimit(100);


		$this->Locations_model->setJoin('locations as locations_cAL', 'locations.parent_id = locations_cAL.id');
		$this->Locations_model->setSelect('locations.*');
		$this->Locations_model->setSelect('locations_cAL.name as parent_name');


 
        $this->template_data->set('locations', $this->Locations_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Locations" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('locations', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_locations')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Locations_model->setOrder('name', 'ASC');$this->Locations_model->setLimit(0);
		$this->template_data->set('locations_parent_id', $this->Locations_model->recursive('parent_id', '0', 2));

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Location" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('locations', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_locations')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Locations_model->setId($id);
	    $current_item = $this->Locations_model->getById();
        $this->template_data->set('locations',  $current_item);
        
        
		$this->Locations_model->setOrder('name', 'ASC');$this->Locations_model->setLimit(0);
		$this->template_data->set('locations_parent_id', $this->Locations_model->recursive('parent_id', '0', 2));

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->name ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('locations', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_locations')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Locations_model->setId($id);
         $this->template_data->set('locations', $this->Locations_model->deleteById() );
         $this->output->set_header("location: " . site_url('locations') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:locations_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Locations_model->setId( $this->input->post('id') );
			$this->Locations_model->setName( $this->input->post('name') );

			if( $action == 'edit' ) {
				$this->Locations_model->setSlug( url_title($this->input->post('slug'), '-', TRUE) );
			} else {
				$this->Locations_model->setSlug(  url_title($this->input->post('name'), '-', TRUE) );
			}

			$this->Locations_model->setParentId( $this->input->post('parent_id') );
			$this->Locations_model->setActive( $this->input->post('active') );

			if( $action == 'add' ) {
				if( $this->Locations_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Locations_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file locations.php */
/* Location: ./application/controllers/locations.php */