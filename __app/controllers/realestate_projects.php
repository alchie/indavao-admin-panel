<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_projects extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_realestate_projects')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('realestate_projects');
        $this->load->model( array('Realestate_projects_model', 'Locations_model') );
        
        $this->template_data->set('main_page', 'ads' ); 
        $this->template_data->set('sub_page', 'realestate_projects' ); 
        $this->template_data->set('page_title', 'Real Estate Projects' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Realestate_projects_model->setFilter( 'realestate_projects.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Realestate_projects_model->setLimit(100);
        
		$this->Realestate_projects_model->setJoin('locations as locations_ILo', 'realestate_projects.location = locations_ILo.id');
		$this->Realestate_projects_model->setSelect('realestate_projects.*');
		$this->Realestate_projects_model->setSelect('locations_ILo.name as location_name');


        $this->template_data->set('realestate_projects', $this->Realestate_projects_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('realestate_projects', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Realestate_projects_model->setStart($start);
	    $this->Realestate_projects_model->setLimit(100);


		$this->Realestate_projects_model->setJoin('locations as locations_ILo', 'realestate_projects.location = locations_ILo.id');
		$this->Realestate_projects_model->setSelect('realestate_projects.*');
		$this->Realestate_projects_model->setSelect('locations_ILo.name as location_name');


 
        $this->template_data->set('realestate_projects', $this->Realestate_projects_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Real Estate Projects" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_realestate_projects')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Locations_model->setOrder('name', 'ASC');$this->Locations_model->setLimit(0);
		$this->template_data->set('locations_location', $this->Locations_model->recursive('parent_id', '0', 3));

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Project" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_realestate_projects')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Realestate_projects_model->setId($id);
	    $current_item = $this->Realestate_projects_model->getById();
        $this->template_data->set('realestate_projects',  $current_item);
        
        
		$this->Locations_model->setOrder('name', 'ASC');$this->Locations_model->setLimit(0);
		$this->template_data->set('locations_location', $this->Locations_model->recursive('parent_id', '0', 3));

        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->name ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('realestate_projects', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_realestate_projects')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Realestate_projects_model->setId($id);
         $this->template_data->set('realestate_projects', $this->Realestate_projects_model->deleteById() );
         $this->output->set_header("location: " . site_url('realestate_projects') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('name', 'lang:realestate_projects_name', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:realestate_projects_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Realestate_projects_model->setId( $this->input->post('id') );
			$this->Realestate_projects_model->setName( $this->input->post('name') );
			$this->Realestate_projects_model->setAddress( $this->input->post('address') );
			$this->Realestate_projects_model->setLocation( $this->input->post('location') );
			$this->Realestate_projects_model->setDevoperName( $this->input->post('devoper_name') );
			$this->Realestate_projects_model->setDeveloperUrl( $this->input->post('developer_url') );
			$this->Realestate_projects_model->setDescription( $this->input->post('description') );
			$this->Realestate_projects_model->setMap( $this->input->post('map') );
			$this->Realestate_projects_model->setLogoUrl( $this->input->post('logo_url') );

			if( $action == 'edit' ) {
				$this->Realestate_projects_model->setSlug( url_title($this->input->post('slug'), '-', TRUE) );
			} else {
				$this->Realestate_projects_model->setSlug(  url_title($this->input->post('name'), '-', TRUE) );
			}

			$this->Realestate_projects_model->setActive( $this->input->post('active') );

			if( $action == 'add' ) {
				if( $this->Realestate_projects_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Realestate_projects_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file realestate_projects.php */
/* Location: ./application/controllers/realestate_projects.php */