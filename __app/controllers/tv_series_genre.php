<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tv_series_genre extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_tv_series_genre')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        if( $this->input->get('series_id') == '') {
                redirect('tv_series','location');
        } 
        $this->template_data->set('filter_key', 'series_id' ); 
        
        $this->load->library('template_data');
        $this->lang->load('tv_series_genre');
        $this->load->model( array('Tv_series_genre_model', 'Tv_series_model', 'Taxonomy_genre_model') );
        
        $this->template_data->set('main_page', 'multimedia' ); 
        $this->template_data->set('sub_page', 'tv_series' ); 
        $this->template_data->set('page_title', 'Genre' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Tv_series_genre_model->setFilter( 'tv_series_genre.'.func_get_arg(0), func_get_arg(1) );
        }
        
        if( $this->input->get('series_id') != '') {
                $this->Tv_series_genre_model->setFilter( 'tv_series_genre.series_id', $this->input->get('series_id') );
        }
        
                $this->Tv_series_model->setSeriesId($this->input->get('series_id'), true);
        $parentData = $this->Tv_series_model->get();
        $this->template_data->page_title( $parentData->series_title );
        
        $this->Tv_series_genre_model->setLimit(100);
        
		$this->Tv_series_genre_model->setJoin('tv_series as tv_series_UQx', 'tv_series_genre.series_id = tv_series_UQx.series_id');
		$this->Tv_series_genre_model->setSelect('tv_series_genre.*');
		$this->Tv_series_genre_model->setSelect('tv_series_UQx.series_title as series_title');


		$this->Tv_series_genre_model->setJoin('taxonomy_genre as taxonomy_genre_oSz', 'tv_series_genre.genre_id = taxonomy_genre_oSz.genre_id');
		$this->Tv_series_genre_model->setSelect('tv_series_genre.*');
		$this->Tv_series_genre_model->setSelect('taxonomy_genre_oSz.genre_name as genre_name');


        $this->template_data->set('tv_series_genre', $this->Tv_series_genre_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('tv_series_genre', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Tv_series_genre_model->setStart($start);
	    $this->Tv_series_genre_model->setLimit(100);


		$this->Tv_series_genre_model->setJoin('tv_series as tv_series_UQx', 'tv_series_genre.series_id = tv_series_UQx.series_id');
		$this->Tv_series_genre_model->setSelect('tv_series_genre.*');
		$this->Tv_series_genre_model->setSelect('tv_series_UQx.series_title as series_title');


		$this->Tv_series_genre_model->setJoin('taxonomy_genre as taxonomy_genre_oSz', 'tv_series_genre.genre_id = taxonomy_genre_oSz.genre_id');
		$this->Tv_series_genre_model->setSelect('tv_series_genre.*');
		$this->Tv_series_genre_model->setSelect('taxonomy_genre_oSz.genre_name as genre_name');


 
        $this->template_data->set('tv_series_genre', $this->Tv_series_genre_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Genre" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_tv_series_genre')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
		$this->Tv_series_model->setOrder('series_title', 'ASC');
		$this->Tv_series_model->setLimit(0);
		$this->Tv_series_model->limitDataFields(array('series_id','series_title'));
		$this->template_data->set('tv_series_series_id', $this->Tv_series_model->populate() );

		$this->Taxonomy_genre_model->setOrder('genre_name', 'ASC');
		$this->Taxonomy_genre_model->setLimit(0);
		$this->Taxonomy_genre_model->limitDataFields(array('genre_id','genre_name'));
		$this->template_data->set('taxonomy_genre_genre_id', $this->Taxonomy_genre_model->populate() );

	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Genre" ); 
	    
        $this->Tv_series_model->setSeriesId($this->input->get('series_id'), true);
        $parentData = $this->Tv_series_model->get();
        $this->template_data->page_title( $parentData->series_title );
            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_tv_series_genre')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Tv_series_genre_model->setId($id);
	    $current_item = $this->Tv_series_genre_model->getById();
        $this->template_data->set('tv_series_genre',  $current_item);
        
        
		$this->Tv_series_model->setOrder('series_title', 'ASC');
		$this->Tv_series_model->setLimit(0);
		$this->Tv_series_model->limitDataFields(array('series_id','series_title'));
		$this->template_data->set('tv_series_series_id', $this->Tv_series_model->populate() );

		$this->Taxonomy_genre_model->setOrder('genre_name', 'ASC');
		$this->Taxonomy_genre_model->setLimit(0);
		$this->Taxonomy_genre_model->limitDataFields(array('genre_id','genre_name'));
		$this->template_data->set('taxonomy_genre_genre_id', $this->Taxonomy_genre_model->populate() );

        
	    $this->template_data->set('action', 'edit' ); 
	    
	            $this->Tv_series_model->setSeriesId($this->input->get('series_id'), true);
        $parentData = $this->Tv_series_model->get();
        $this->template_data->page_title( $parentData->series_title );
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('tv_series_genre', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_tv_series_genre')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Tv_series_genre_model->setId($id);
         $this->template_data->set('tv_series_genre', $this->Tv_series_genre_model->deleteById() );
         $this->output->set_header("location: " . site_url('tv_series_genre') . "?series_id=" . $this->input->get('series_id'));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('series_id', 'lang:tv_series_genre_series_id', 'required');
			$this->form_validation->set_rules('genre_id', 'lang:tv_series_genre_genre_id', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:tv_series_genre_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Tv_series_genre_model->setId( $this->input->post('id') );
			$this->Tv_series_genre_model->setSeriesId( $this->input->post('series_id') );
			$this->Tv_series_genre_model->setGenreId( $this->input->post('genre_id') );

			if( $action == 'add' ) {
				if( $this->Tv_series_genre_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Tv_series_genre_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file tv_series_genre.php */
/* Location: ./application/controllers/tv_series_genre.php */