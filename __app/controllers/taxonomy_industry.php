<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taxonomy_industry extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if(  ! $this->session->userdata('controller_taxonomy_industry')   ) {
			redirect('dashboard/unauthorized','location');
		}
		
        
        
        $this->load->library('template_data');
        $this->lang->load('taxonomy_industry');
        $this->load->model( array('Taxonomy_industry_model') );
        
        $this->template_data->set('main_page', 'taxonomy' ); 
        $this->template_data->set('sub_page', 'taxonomy_industry' ); 
        $this->template_data->set('page_title', 'Industry' ); 

    }
    
    public function index()
	{
        
        $numargs = func_num_args();
        if ($numargs == 2) {
            $this->Taxonomy_industry_model->setFilter( 'taxonomy_industry.'.func_get_arg(0), func_get_arg(1) );
        }
        
        
        
        
        
        $this->Taxonomy_industry_model->setLimit(100);
        
        $this->template_data->set('taxonomy_industry', $this->Taxonomy_industry_model->populate() );
        
        $this->template_data->set('action', NULL ); 
        $this->load->view('common_head', $this->template_data->get() );
        $this->load->view('common_header', $this->template_data->get() );
        $this->load->view('taxonomy_industry', $this->template_data->get() );
        $this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function page($start)
	{	    
	    $this->Taxonomy_industry_model->setStart($start);
	    $this->Taxonomy_industry_model->setLimit(100);


 
        $this->template_data->set('taxonomy_industry', $this->Taxonomy_industry_model->populate() );
        
	    $this->template_data->set('action', NULL ); 
	    $this->template_data->page_title( "Page {$start} - Industry" ); 
	     
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_industry', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function add()
	{
	
		if( ! $this->session->userdata('controller_taxonomy_industry')->can_add ) {
			redirect('dashboard/unauthorized','location');
		}
		
	    if( $this->input->post() ) {
	        $this->submission('add');
	    }
	    
	    
	    
	    $this->template_data->set('action', 'add' ); 
	    $this->template_data->page_title( "Add New Industry" ); 
	    

            
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_industry', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function edit($id)
	{
		if( ! $this->session->userdata('controller_taxonomy_industry')->can_edit ) {
			redirect('dashboard/unauthorized','location');
		}
		
		if( $this->input->post() ) {
	        $this->submission('edit');
	    }
	    
	    $this->Taxonomy_industry_model->setId($id);
	    $current_item = $this->Taxonomy_industry_model->getById();
        $this->template_data->set('taxonomy_industry',  $current_item);
        
        
        
	    $this->template_data->set('action', 'edit' ); 
	    
	    $this->template_data->page_title( $current_item->name ); 
	    	    
		$this->load->view('common_head', $this->template_data->get() );
		$this->load->view('common_header', $this->template_data->get() );
		$this->load->view('taxonomy_industry', $this->template_data->get() );
		$this->load->view('common_foot', $this->template_data->get() );	
		
	}
	
	public function delete($id)
	{
		if( ! $this->session->userdata('controller_taxonomy_industry')->can_delete ) {
			redirect('dashboard/unauthorized','location');
		}
		
         $this->Taxonomy_industry_model->setId($id);
         $this->template_data->set('taxonomy_industry', $this->Taxonomy_industry_model->deleteById() );
         $this->output->set_header("location: " . site_url('taxonomy_industry') . "?=" . $this->input->get(''));
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    

		if( $action == 'add' ) {
			$this->form_validation->set_rules('name', 'lang:taxonomy_industry_name', 'required');

		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:taxonomy_industry_id', 'required');

		}


		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
			}
		} 
		else 
		{
			$this->Taxonomy_industry_model->setId( $this->input->post('id') );
			$this->Taxonomy_industry_model->setName( $this->input->post('name') );
			$this->Taxonomy_industry_model->setDescription( $this->input->post('description') );

			if( $action == 'edit' ) {
				$this->Taxonomy_industry_model->setSlug( url_title($this->input->post('slug'), '-', TRUE) );
			} else {
				$this->Taxonomy_industry_model->setSlug(  url_title($this->input->post('name'), '-', TRUE) );
			}

			$this->Taxonomy_industry_model->setActive( $this->input->post('active') );

			if( $action == 'add' ) {
				if( $this->Taxonomy_industry_model->insert() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
				}
			} 
			elseif( $action == 'edit' ) {
				if( $this->Taxonomy_industry_model->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
				}
			}
		}

	    
	}
	
}
/* End of file taxonomy_industry.php */
/* Location: ./application/controllers/taxonomy_industry.php */